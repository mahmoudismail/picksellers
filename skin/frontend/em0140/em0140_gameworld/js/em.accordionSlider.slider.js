(function($) {
    function getWindowWidth() {
        var w = window;
        var d = document;
        var e = d.documentElement;
        var g = d.getElementsByTagName('body')[0];
        var x = w.innerWidth||e.clientWidth||g.clientWidth;
    	return x;
    };
    
    var emgameworld_AccordionSlider = {
		renderSlideshow: function() {
			var accordion = $('#em-accordion-slider');	            
            var $_wi = getWindowWidth();
            var $_visiblePanels = 3;
            var $_height = 600;
            var $_autoplay = true;
            if($_wi>1025){
                $_visiblePanels = 3;
                $_height = 600;
            }else{
                if($_wi > 767){
                    $_visiblePanels = 2;
                    $_height = 500;
                }else{
                    if($_wi > 480){
                        $_visiblePanels = 2;
                        $_height = 400;
                    }else{
                        $_visiblePanels = 1;
                        $_height = 300;
                    }
                    
                }
                $_autoplay = false;
            }
            var $_css = $('#em-accordion-slider-css');
            $_css.empty();
            $_css.html('<style type="text/css">#em-accordion-slider {height:' + $_height + 'px !important;}</style>');
            accordion.accordionSlider('closePanels');            
    		accordion.accordionSlider({
    			width: $_wi,
    			height: $_height,
    			responsiveMode: 'auto',
    			openedPanelSize: 'max',
    			maxOpenedPanelSize: '100%',
    			visiblePanels: $_visiblePanels,
                startPanel: 0,
    			closePanelsOnMouseOut: false,
    			autoplay: $_autoplay,
                mouseWheel: false,
                breakpoints: {
    				1024: {visiblePanels: 2, height: 500, aspectRatio: 1,},
    				650: {visiblePanels: 2, height: 400, aspectRatio: 1,},
                    320: {visiblePanels: 1, height: 300, aspectRatio: 1,},
    			}
    		});            
		},
	};
	jQuery(document).ready(function() {
        emgameworld_AccordionSlider.renderSlideshow();
		var accordion = $('#em-accordion-slider');	
        var $_btnNext = $('.em-accordion-slider-wrapper').find('.em-accordion-next');
        var $_btnPre = 	$('.em-accordion-slider-wrapper').find('.em-accordion-pre');
		$_btnNext.click(function(e){
            e.preventDefault();
			accordion.accordionSlider('nextPanel');  
		});
		$_btnPre.click(function(e){
            e.preventDefault();
            accordion.accordionSlider('previousPanel');
		});	
	});    
    $(window).resize($.throttle(300,function(){  
        emgameworld_AccordionSlider.renderSlideshow();              	   
    })); 
})(jQuery);