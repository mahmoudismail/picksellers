<?php
class EM_Em0140settings_Model_Config_Tabsposition
{
	/**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'below_info', 'label'=>Mage::helper('adminhtml')->__('Below Product Information')),
            array('value' => 'below_img', 'label'=>Mage::helper('adminhtml')->__('Below Product Image')),
        );
    }
}
?>