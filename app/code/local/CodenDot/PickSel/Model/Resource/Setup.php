<?php
/**
 * CodenDot_PickSel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       CodenDot
 * @package        CodenDot_PickSel
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * PickSel setup
 *
 * @category    CodenDot
 * @package     CodenDot_PickSel
 * @author      Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
 */
class CodenDot_PickSel_Model_Resource_Setup extends Mage_Catalog_Model_Resource_Setup
{

    /**
     * get the default entities for picksel module - used at installation
     *
     * @access public
     * @return array()
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function getDefaultEntities()
    {
        $entities = array();
        $entities['codendot_picksel_supplier'] = array(
            'entity_model'                  => 'codendot_picksel/supplier',
            'attribute_model'               => 'codendot_picksel/resource_eav_attribute',
            'table'                         => 'codendot_picksel/supplier',
            'additional_attribute_table'    => 'codendot_picksel/eav_attribute',
            'entity_attribute_collection'   => 'codendot_picksel/supplier_attribute_collection',
            'attributes'                    => array(
                    'supplier_name' => array(
                        'group'          => 'General',
                        'type'           => 'varchar',
                        'backend'        => '',
                        'frontend'       => '',
                        'label'          => 'Name',
                        'input'          => 'text',
                        'source'         => '',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'required'       => '1',
                        'user_defined'   => true,
                        'default'        => '',
                        'unique'         => false,
                        'position'       => '10',
                        'note'           => 'name of supplier',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '0',
                    ),
                    'supplier_xml_file' => array(
                        'group'          => 'General',
                        'type'           => 'varchar',
                        'backend'        => 'codendot_picksel/supplier_attribute_backend_file',
                        'frontend'       => '',
                        'label'          => 'Supplier XML File',
                        'input'          => 'file',
                        'source'         => '',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'required'       => '',
                        'user_defined'   => true,
                        'default'        => '',
                        'unique'         => false,
                        'position'       => '20',
                        'note'           => 'xml file for supplier',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '0',
                    ),
                    'supplier_url' => array(
                        'group'          => 'General',
                        'type'           => 'varchar',
                        'backend'        => '',
                        'frontend'       => '',
                        'label'          => 'URL',
                        'input'          => 'text',
                        'source'         => '',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'required'       => '0',
                        'user_defined'   => true,
                        'default'        => '',
                        'unique'         => false,
                        'position'       => '30',
                        'note'           => 'URL for online xml',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '0',
                    ),
                    'supplier_new_price' => array(
                        'group'          => 'General',
                        'type'           => 'varchar',
                        'backend'        => '',
                        'frontend'       => '',
                        'label'          => 'New Price',
                        'input'          => 'text',
                        'source'         => '',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'required'       => '1',
                        'user_defined'   => false,
                        'default'        => '',
                        'unique'         => false,
                        'position'       => '40',
                        'note'           => 'Your new price',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '0',
                    ),
                    'supplier_address' => array(
                        'group'          => 'General',
                        'type'           => 'varchar',
                        'backend'        => '',
                        'frontend'       => '',
                        'label'          => 'Address',
                        'input'          => 'text',
                        'source'         => '',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'required'       => '0',
                        'user_defined'   => true,
                        'default'        => '',
                        'unique'         => false,
                        'position'       => '50',
                        'note'           => 'Supplier Address',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '0',
                    ),
                    'supplier_descp' => array(
                        'group'          => 'General',
                        'type'           => 'text',
                        'backend'        => '',
                        'frontend'       => '',
                        'label'          => 'Description',
                        'input'          => 'textarea',
                        'source'         => '',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'required'       => '1',
                        'user_defined'   => true,
                        'default'        => '',
                        'unique'         => false,
                        'position'       => '60',
                        'note'           => 'Supplier description',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '1',
                    ),
                    'supplier_time_update' => array(
                        'group'          => 'General',
                        'type'           => 'int',
                        'backend'        => '',
                        'frontend'       => '',
                        'label'          => 'Supplier time Update',
                        'input'          => 'select',
                        'source'         => 'eav/entity_attribute_source_table',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'required'       => '0',
                        'user_defined'   => true,
                        'default'        => '',
                        'unique'         => false,
                        'position'       => '70',
                        'note'           => 'supplier update time',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '0',
                        'option' =>
                            array (
                                'values' =>
                                    array (
                                        '0 * * * *',
                                        '0 0 * * *',
                                    ),
                                ),
                    ),
                    'supplier_format' => array(
                        'group'          => 'General',
                        'type'           => 'text',
                        'backend'        => '',
                        'frontend'       => '',
                        'label'          => 'Format',
                        'input'          => 'textarea',
                        'source'         => '',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'required'       => '0',
                        'user_defined'   => true,
                        'default'        => '',
                        'unique'         => false,
                        'position'       => '80',
                        'note'           => '',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '0',
                    ),
                    'status' => array(
                        'group'          => 'General',
                        'type'           => 'int',
                        'backend'        => '',
                        'frontend'       => '',
                        'label'          => 'Enabled',
                        'input'          => 'select',
                        'source'         => 'eav/entity_attribute_source_boolean',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'required'       => '',
                        'user_defined'   => false,
                        'default'        => '1',
                        'unique'         => false,
                        'position'       => '90',
                        'note'           => '',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '0',
                    ),

                )
         );
        return $entities;
    }
}
