<?php
/**
 * CodenDot_PickSel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       CodenDot
 * @package        CodenDot_PickSel
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier attribute collection model
 *
 * @category    CodenDot
 * @package     CodenDot_PickSel
 * @author      Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
 */
class CodenDot_PickSel_Model_Resource_Supplier_Attribute_Collection extends Mage_Eav_Model_Resource_Entity_Attribute_Collection
{
    /**
     * init attribute select
     *
     * @access protected
     * @return CodenDot_PickSel_Model_Resource_Supplier_Attribute_Collection
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    protected function _initSelect()
    {
        $this->getSelect()->from(array('main_table' => $this->getResource()->getMainTable()))
            ->where(
                'main_table.entity_type_id=?',
                Mage::getModel('eav/entity')->setType('codendot_picksel_supplier')->getTypeId()
            )
            ->join(
                array('additional_table' => $this->getTable('codendot_picksel/eav_attribute')),
                'additional_table.attribute_id=main_table.attribute_id'
            );
        return $this;
    }

    /**
     * set entity type filter
     *
     * @access public
     * @param string $typeId
     * @return CodenDot_PickSel_Model_Resource_Supplier_Attribute_Collection
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function setEntityTypeFilter($typeId)
    {
        return $this;
    }

    /**
     * Specify filter by "is_visible" field
     *
     * @access public
     * @return CodenDot_PickSel_Model_Resource_Supplier_Attribute_Collection
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function addVisibleFilter()
    {
        return $this->addFieldToFilter('additional_table.is_visible', 1);
    }

    /**
     * Specify filter by "is_editable" field
     *
     * @access public
     * @return CodenDot_PickSel_Model_Resource_Supplier_Attribute_Collection
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function addEditableFilter()
    {
        return $this->addFieldToFilter('additional_table.is_editable', 1);
    }
}
