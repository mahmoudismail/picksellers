<?php
/**
 * CodenDot_PickSel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       CodenDot
 * @package        CodenDot_PickSel
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin search model
 *
 * @category    CodenDot
 * @package     CodenDot_PickSel
 * @author      Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
 */
class CodenDot_PickSel_Model_Adminhtml_Search_Supplier extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return CodenDot_PickSel_Model_Adminhtml_Search_Supplier
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('codendot_picksel/supplier_collection')
            ->addAttributeToFilter('supplier_new_price', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $supplier) {
            $arr[] = array(
                'id'          => 'supplier/1/'.$supplier->getId(),
                'type'        => Mage::helper('codendot_picksel')->__('Supplier'),
                'name'        => $supplier->getSupplierNewPrice(),
                'description' => $supplier->getSupplierNewPrice(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/picksel_supplier/edit',
                    array('id'=>$supplier->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
