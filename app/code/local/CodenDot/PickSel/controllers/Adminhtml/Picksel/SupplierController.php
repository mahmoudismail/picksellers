<?php
/**
 * CodenDot_PickSel extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       CodenDot
 * @package        CodenDot_PickSel
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier admin controller
 *
 * @category    CodenDot
 * @package     CodenDot_PickSel
 * @author      Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
 */
class CodenDot_PickSel_Adminhtml_Picksel_SupplierController extends Mage_Adminhtml_Controller_Action
{
    /**
     * constructor - set the used module name
     *
     * @access protected
     * @return void
     * @see Mage_Core_Controller_Varien_Action::_construct()
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    protected function _construct()
    {
        $this->setUsedModuleName('CodenDot_PickSel');
    }

    /**
     * init the supplier
     *
     * @access protected
     * @return CodenDot_PickSel_Model_Supplier
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    protected function _initSupplier()
    {
        $this->_title($this->__('PickSellers'))
             ->_title($this->__('Manage Suppliers'));

        $supplierId  = (int) $this->getRequest()->getParam('id');
        $supplier    = Mage::getModel('codendot_picksel/supplier')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if ($supplierId) {
            $supplier->load($supplierId);
        }
        Mage::register('current_supplier', $supplier);
        return $supplier;
    }

    /**
     * default action for supplier controller
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function indexAction()
    {
        $this->_title($this->__('PickSellers'))
             ->_title($this->__('Manage Suppliers'));
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * new supplier action
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * edit supplier action
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function editAction()
    {
        $supplierId  = (int) $this->getRequest()->getParam('id');
        $supplier    = $this->_initSupplier();
        if ($supplierId && !$supplier->getId()) {
            $this->_getSession()->addError(
                Mage::helper('codendot_picksel')->__('This supplier no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        if ($data = Mage::getSingleton('adminhtml/session')->getSupplierData(true)) {
            $supplier->setData($data);
        }
        $this->_title($supplier->getSupplierNewPrice());
        Mage::dispatchEvent(
            'codendot_picksel_supplier_edit_action',
            array('supplier' => $supplier)
        );
        $this->loadLayout();
        if ($supplier->getId()) {
            if (!Mage::app()->isSingleStoreMode() && ($switchBlock = $this->getLayout()->getBlock('store_switcher'))) {
                $switchBlock->setDefaultStoreName(Mage::helper('codendot_picksel')->__('Default Values'))
                    ->setWebsiteIds($supplier->getWebsiteIds())
                    ->setSwitchUrl(
                        $this->getUrl(
                            '*/*/*',
                            array(
                                '_current'=>true,
                                'active_tab'=>null,
                                'tab' => null,
                                'store'=>null
                            )
                        )
                    );
            }
        } else {
            $this->getLayout()->getBlock('left')->unsetChild('store_switcher');
        }
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->renderLayout();
    }

    /**
     * save supplier action
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function saveAction()
    {
        $storeId        = $this->getRequest()->getParam('store');
        $redirectBack   = $this->getRequest()->getParam('back', false);
        $supplierId   = $this->getRequest()->getParam('id');
        $isEdit         = (int)($this->getRequest()->getParam('id') != null);
        $data = $this->getRequest()->getPost();
        if ($data) {
            $supplier     = $this->_initSupplier();
            $supplierData = $this->getRequest()->getPost('supplier', array());
            $supplier->addData($supplierData);
            $supplier->setAttributeSetId($supplier->getDefaultAttributeSetId());
            if ($useDefaults = $this->getRequest()->getPost('use_default')) {
                foreach ($useDefaults as $attributeCode) {
                    $supplier->setData($attributeCode, false);
                }
            }
            try {
                $supplier->save();
                $supplierId = $supplier->getId();
                $this->_getSession()->addSuccess(
                    Mage::helper('codendot_picksel')->__('Supplier was saved')
                );
            } catch (Mage_Core_Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage())
                    ->setSupplierData($supplierData);
                $redirectBack = true;
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError(
                    Mage::helper('codendot_picksel')->__('Error saving supplier')
                )
                ->setSupplierData($supplierData);
                $redirectBack = true;
            }
        }
        if ($redirectBack) {
            $this->_redirect(
                '*/*/edit',
                array(
                    'id'    => $supplierId,
                    '_current'=>true
                )
            );
        } else {
            $this->_redirect('*/*/', array('store'=>$storeId));
        }
    }

    /**
     * delete supplier
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            $supplier = Mage::getModel('codendot_picksel/supplier')->load($id);
            try {
                $supplier->delete();
                $this->_getSession()->addSuccess(
                    Mage::helper('codendot_picksel')->__('The suppliers has been deleted.')
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->getResponse()->setRedirect(
            $this->getUrl('*/*/', array('store'=>$this->getRequest()->getParam('store')))
        );
    }

    /**
     * mass delete suppliers
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function massDeleteAction()
    {
        $supplierIds = $this->getRequest()->getParam('supplier');
        if (!is_array($supplierIds)) {
            $this->_getSession()->addError($this->__('Please select suppliers.'));
        } else {
            try {
                foreach ($supplierIds as $supplierId) {
                    $supplier = Mage::getSingleton('codendot_picksel/supplier')->load($supplierId);
                    Mage::dispatchEvent(
                        'codendot_picksel_controller_supplier_delete',
                        array('supplier' => $supplier)
                    );
                    $supplier->delete();
                }
                $this->_getSession()->addSuccess(
                    Mage::helper('codendot_picksel')->__('Total of %d record(s) have been deleted.', count($supplierIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function massStatusAction()
    {
        $supplierIds = $this->getRequest()->getParam('supplier');
        if (!is_array($supplierIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('codendot_picksel')->__('Please select suppliers.')
            );
        } else {
            try {
                foreach ($supplierIds as $supplierId) {
                $supplier = Mage::getSingleton('codendot_picksel/supplier')->load($supplierId)
                    ->setStatus($this->getRequest()->getParam('status'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d suppliers were successfully updated.', count($supplierIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('codendot_picksel')->__('There was an error updating suppliers.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * restrict access
     *
     * @access protected
     * @return bool
     * @see Mage_Adminhtml_Controller_Action::_isAllowed()
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('codendot_picksel/supplier');
    }

    /**
     * Export suppliers in CSV format
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function exportCsvAction()
    {
        $fileName   = 'suppliers.csv';
        $content    = $this->getLayout()->createBlock('codendot_picksel/adminhtml_supplier_grid')
            ->getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export suppliers in Excel format
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function exportExcelAction()
    {
        $fileName   = 'supplier.xls';
        $content    = $this->getLayout()->createBlock('codendot_picksel/adminhtml_supplier_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export suppliers in XML format
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function exportXmlAction()
    {
        $fileName   = 'supplier.xml';
        $content    = $this->getLayout()->createBlock('codendot_picksel/adminhtml_supplier_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * wysiwyg editor action
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function wysiwygAction()
    {
        $elementId     = $this->getRequest()->getParam('element_id', md5(microtime()));
        $storeId       = $this->getRequest()->getParam('store_id', 0);
        $storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

        $content = $this->getLayout()->createBlock(
            'codendot_picksel/adminhtml_picksel_helper_form_wysiwyg_content',
            '',
            array(
                'editor_element_id' => $elementId,
                'store_id'          => $storeId,
                'store_media_url'   => $storeMediaUrl,
            )
        );
        $this->getResponse()->setBody($content->toHtml());
    }

    /**
     * mass Supplier time Update change
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function massSupplierTimeUpdateAction()
    {
        $supplierIds = (array)$this->getRequest()->getParam('supplier');
        $storeId       = (int)$this->getRequest()->getParam('store', 0);
        $flag          = (int)$this->getRequest()->getParam('flag_supplier_time_update');
        if ($flag == 2) {
            $flag = 0;
        }
        try {
            foreach ($supplierIds as $supplierId) {
                $supplier = Mage::getSingleton('codendot_picksel/supplier')
                    ->setStoreId($storeId)
                    ->load($supplierId);
                $supplier->setSupplierTimeUpdate($flag)->save();
            }
            $this->_getSession()->addSuccess(
                Mage::helper('codendot_picksel')->__('Total of %d record(s) have been updated.', count($supplierIds))
            );
        } catch (Mage_Core_Model_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addException(
                $e,
                Mage::helper('codendot_picksel')->__('An error occurred while updating the suppliers.')
            );
        }
        $this->_redirect('*/*/', array('store'=> $storeId));
    }


    /**
     * Debug Function class name for log file
     *
     * @access public
     * @return string
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    protected function debugFileName() {
      $bt = debug_backtrace();
      $caller = array_shift($bt);
      $line = $caller['line'];
      $file = $caller['file'];
      $class = $caller['class'];
      $data = $class.' at '.$line.' in '.$file;
      return $data;
    }

    /**
     * Supplier XML Format
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function supplierMappingAction()
    {
        //$supplierIds = (array)$this->getRequest()->getParam('supplier');
        $supplierId  = (int) $this->getRequest()->getParam('id');
        // if (!is_array($supplierIds)) {
        //     $this->_getSession()->addError($this->__('Please select suppliers.'));
        // } else {
            try {
              $this->_title($this->__("Supplier XML Format"));
              $this->loadLayout();
              $this->_setActiveMenu('codendot_picksel');
              $layout = $this->getLayout();
              $block = $layout->getBlock('suppliermapping');
              if($block) {
                  $block->setSupplierID($supplierId);
              }
              $this->renderLayout();
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        //}
    }


    /**
     * Categories Mapping
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function categoriesMappingAction()
    {
      $supplierId  = (int) $this->getRequest()->getParam('id');
      try {
        $this->_title($this->__("Category Mapping"));
        $this->loadLayout();
        $this->_setActiveMenu('codendot_picksel');
        $layout = $this->getLayout();
        $block = $layout->getBlock('categoriesmapping');
        if($block) {
            $block->setSupplierID($supplierId);
        }
        $this->renderLayout();
      } catch (Exception $e) {
          $this->_getSession()->addError($e->getMessage());
      }
    }

    /**
     * Supplier Fetch Products
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function supplierFetchProductsAction()
    {
        //$supplierIds = (array)$this->getRequest()->getParam('supplier');
        $supplierId  = (int) $this->getRequest()->getParam('id');
        // if (!is_array($supplierIds)) {
        //     $this->_getSession()->addError($this->__('Please select suppliers.'));
        // } else {
            try {
              $this->_title($this->__("Supplier Fetch Products"));
              $this->loadLayout();
              $this->_setActiveMenu('codendot_picksel');
              $layout = $this->getLayout();
              $block = $layout->getBlock('supplierfetchproducts');
              if($block) {
                  $block->setSupplierID($supplierId);
              }
              $this->renderLayout();
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        //}
    }

    /**
     * Supplier Translate
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function supplierTranslationsAction()
    {
      $supplierId  = (int) $this->getRequest()->getParam('id');
      try {
        $this->_title($this->__("Supplier Translations"));
        $this->loadLayout();
        $this->_setActiveMenu('codendot_picksel');
        $layout = $this->getLayout();
        $block = $layout->getBlock('suppliertranslations');
        if($block) {
            $block->setSupplierID($supplierId);
        }
        $this->renderLayout();
      } catch (Exception $e) {
          $this->_getSession()->addError($e->getMessage());
      }
    }

    /**
     * Supplier Category Translations
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function categoryTranslationsAction()
    {
      $supplierId  = (int) $this->getRequest()->getParam('id');
      try {
        $this->_title($this->__("Supplier Category Translations"));
        $this->loadLayout();
        $this->_setActiveMenu('codendot_picksel');
        $layout = $this->getLayout();
        $block = $layout->getBlock('categorytranslations');
        if($block) {
            $block->setSupplierID($supplierId);
        }
        $this->renderLayout();
      } catch (Exception $e) {
          $this->_getSession()->addError($e->getMessage());
      }
    }

    /**
     * Get Attributes ( Supplier , Default )
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function getAttributesAction() {
      $supplierId  = (int) $this->getRequest()->getParam('supplierId');
      $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
      if($supplier->getStatus()){
        $xmlPath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'supplier/file'.$supplier->getSupplierXmlFile();
        $xmlURL = $supplier->getSupplierUrl();
        $translation = (array) json_decode($supplier->getSupplierXmlTranslations());
        $contents = $this->curl_get_file_contents($xmlURL);
        if ($contents) {
          $xmlString = $contents;
          //$cleanResponse = preg_replace('/&nbsp;|<br \/>/i', '', $xmlString);
          $newContent = str_replace(array("\n", "\r", "\t", "<br>","<b>"), '', $xmlString);
          $newContent = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $newContent);
          $newContent = trim(str_replace('"', "'", $newContent));
          $simpleXml = simplexml_load_string($newContent,null,LIBXML_COMPACT | LIBXML_NOCDATA | LIBXML_PARSEHUGE | LIBXML_NOEMPTYTAG);
          if($simpleXml !==  FALSE)
          {
            $xmlArray = array ();
            $xmlChild = array ();
            $xmlSubChild = array ();

            $xmlChild[] = array(
              'child' =>  $simpleXml->getName()
            );
            foreach($simpleXml->children() as $child) {
              $xmlArray = array(
                'root' => $simpleXml->getName(),
                'parent' => $child->getName()
              );
              $xmlChild[] = array(
                'child' =>  $child->getName()
              );
              foreach($child->children() as $subchild) {
                $xmlChild[] = array(
                  'child' =>  $subchild->getName()
                );
                foreach ($subchild->children() as $subsubchild) {
                  $xmlChild[] = array(
                    'child' =>  $subchild->getName().'.*.'.$subsubchild->getName()
                  );
                  foreach ($subsubchild->children() as $sub) {
                    $xmlChild[] = array(
                      'child' =>  $subchild->getName().'.'.$subsubchild->getName().'.*.'.$sub->getName()
                    );
                  }
                }
              }
              $xmlArray['child'] = $xmlChild;
              //break;
            }
            $supplierAttribute = array();
            for ($i=0; $i < count($xmlArray['child']) ; $i++) {
              $supplierAttribute[] = $xmlArray['child'][$i]['child'];
            }

            $Array = array_unique($supplierAttribute);
            $uniqueArray = '';
            $result = [];
            if(!empty($translation)) {
              foreach ($Array as $key => $value) {
                $index = array_search($value,$translation);
                //echo '--> val = '.$value.' ->> '.$index.'<br>';
                $uniqueArray[$key] = $value.'>>'.$index;
                $result['translation'] = 1;
                $result['error'] = 0;
              }
            }else {
              $uniqueArray = array_unique($supplierAttribute);
              $result['translation'] = 0;
              $result['error'] = 0;
              echo json_encode($result);
            }
            $result['supplier'] = $uniqueArray;
            $result['default'] = $this->getAllAttributesAction();
            echo json_encode($result);
          }else {
            foreach(libxml_get_errors() as $error) {
              Mage::log($this->debugFileName().' - Failed loading XML Error Message: '.$error->message, null, 'picksellers.log');
            }
            $result['error'] = 1;
            $result['msg'] = $this->__('Failed loading XML for some reason.');
            echo json_encode($result);
            Mage::log($this->debugFileName().' - Message: '.$xmlResult['data'], null, 'picksellers.log');
          }
        }else {
          //return false;
          $result['error'] = 1;
          $result['msg'] = $this->__('No Data found in xml.');
          echo json_encode($result);
        }
      }else {
        $result['error'] = 1;
        $result['msg'] = $this->__('Your supplier not activated yet.');
        echo json_encode($result);
      }
  }

  /**
 * Get Attributes ( Supplier , Default )
 *
 * @access public
 * @return void
 * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
 */
public function getAttributesTranslationsAction() {
  $supplierId  = (int) $this->getRequest()->getParam('supplierId');
  $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
  if($supplier->getStatus()){
    $xmlPath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'supplier/file'.$supplier->getSupplierXmlFile();
    $xmlURL = $supplier->getSupplierUrl();
    $translation = (array) json_decode($supplier->getSupplierXmlTranslations());
    $contents = $this->curl_get_file_contents($xmlURL);
    if ($contents) {
      $xmlString = $contents;
      //$cleanResponse = preg_replace('/&nbsp;|<br \/>/i', '', $xmlString);
      $newContent = str_replace(array("\n", "\r", "\t", "<br>","<b>"), '', $xmlString);
      $newContent = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $newContent);
      $newContent = trim(str_replace('"', "'", $newContent));
      $simpleXml = simplexml_load_string($newContent,null,LIBXML_COMPACT | LIBXML_NOCDATA | LIBXML_PARSEHUGE | LIBXML_NOEMPTYTAG);
      if($simpleXml !==  FALSE)
      {
        $xmlArray = array ();
        $xmlChild = array ();
        $xmlSubChild = array ();

        $xmlChild[] = array(
          'child' =>  $simpleXml->getName()
        );
        foreach($simpleXml->children() as $child) {
          $xmlArray = array(
            'root' => $simpleXml->getName(),
            'parent' => $child->getName()
          );
          $xmlChild[] = array(
            'child' =>  $child->getName()
          );
          foreach($child->children() as $subchild) {
            $xmlChild[] = array(
              'child' =>  $subchild->getName()
            );
            foreach ($subchild->children() as $subsubchild) {
              $xmlChild[] = array(
                'child' =>  $subchild->getName().'.*.'.$subsubchild->getName()
              );
              foreach ($subsubchild->children() as $sub) {
                $xmlChild[] = array(
                  'child' =>  $subchild->getName().'.'.$subsubchild->getName().'.*.'.$sub->getName()
                );
              }
            }
          }
          $xmlArray['child'] = $xmlChild;
          //break;
        }
        $supplierAttribute = array();
        for ($i=0; $i < count($xmlArray['child']) ; $i++) {
          $supplierAttribute[] = $xmlArray['child'][$i]['child'];
        }

        $Array = array_unique($supplierAttribute);
        $uniqueArray = '';
        $result = [];
        if(!empty($translation)) {
          foreach ($Array as $key => $value) {
            $index = array_search($value,$translation);
            //echo '--> val = '.$value.' ->> '.$index.'<br>';
            $uniqueArray[$key] = $value;//$value.'=>'.$index;
            $result['translation'] = 1;
            $result['error'] = 0;
          }
        }else {
          $uniqueArray = array_unique($supplierAttribute);
          $result['translation'] = 0;
          $result['error'] = 0;
        }
        $result['supplier'] = $uniqueArray;
        $result['default'] = $this->getAllAttributesAction();
        echo json_encode($result);
      }else {
        foreach(libxml_get_errors() as $error) {
          Mage::log($this->debugFileName().' - Failed loading XML Error Message: '.$error->message, null, 'picksellers.log');
        }
        $result['error'] = 1;
        $result['msg'] = $this->__('Failed loading XML for some reason.');
        echo json_encode($result);
        Mage::log($this->debugFileName().' - Message: '.$xmlResult['data'], null, 'picksellers.log');
      }
    }else {
      //return false;
      $result['error'] = 1;
      $result['msg'] = $this->__('No Data found in xml.');
      echo json_encode($result);
    }
  }else {
    $result['error'] = 1;
    $result['msg'] = $this->__('Your supplier not activated yet.');
    echo json_encode($result);
  }
}

  public function getSupplierXmlValueByNodeAction() {
    $supplierCategory = array();
    $supplierId  = (int) $this->getRequest()->getParam('supplierId');
    $SelectedRootCategory  = json_decode($this->getRequest()->getParam('rootCategory'));
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);

    if($supplier->getStatus()){
      $xmlPath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'supplier/file'.$supplier->getSupplierXmlFile();
      $xmlURL = $supplier->getSupplierUrl();
      $translation = (array) json_decode($supplier->getSupplierXmlTranslations());
      $contents = $this->curl_get_file_contents($xmlURL);
      if ($contents) {
        $xmlString = $contents;
        $newContent = str_replace(array("\n", "\r", "\t", "<br>","<b>"), '', $xmlString);
        $newContent = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $newContent);
        $newContent = trim(str_replace('"', "'", $newContent));
        libxml_use_internal_errors(true);
        $simpleXml = simplexml_load_string($newContent,null,LIBXML_COMPACT | LIBXML_NOCDATA | LIBXML_PARSEHUGE | LIBXML_NOEMPTYTAG);
        if($simpleXml !==  FALSE)
        {
          $body = $simpleXml;
          foreach($body->children() as $cat) {
            if(is_array($SelectedRootCategory) && !empty($SelectedRootCategory)) {
              if(count($SelectedRootCategory) > 1) { // array contain main category and sub category
                foreach ($SelectedRootCategory as $key => $value) {
                  $supplierCategory[] = $cat->$value ? $cat->$value : null;
                }
              }else { // array contain main category only
                $supplierCategory[] = $cat->$SelectedRootCategory[0] ? $cat->$SelectedRootCategory[0] : null;
              }
            }
          }
          $result = array();
          $data = [];
          $res = $this->xml2array($supplierCategory);
          foreach ($res as $sub) { // Loop outer array
            foreach ($sub as $val) { // Loop inner arrays
              $val = trim($val);
              if (!in_array($val, $result)) { // Check for duplicates
                $result[] = $val; // Add to result array
              }
            }
          }
          if($result) {
            $data['success'] = true;
            $data['data'] = array_filter($result);
          }else {
            $data['success'] = false;
            $data['data'] = null;
          }
          echo json_encode($data);
        }
      }
    }
  }

  public function getSupplierXmlValueMappingByNodeAction() {
    $supplierCategory = array();
    $supplierId  = (int) $this->getRequest()->getParam('supplierId');
    $SelectedRootCategory  = json_decode($this->getRequest()->getParam('rootCategory'));
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);

    if($supplier->getStatus()){
      $xmlPath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'supplier/file'.$supplier->getSupplierXmlFile();
      $xmlURL = $supplier->getSupplierUrl();
      $categoryTranslation = (array) json_decode($supplier->getSupplierXmlCatTranslations());
      $contents = $this->curl_get_file_contents($xmlURL);
      if ($contents) {
        $xmlString = $contents;
        $newContent = str_replace(array("\n", "\r", "\t", "<br>","<b>"), '', $xmlString);
        $newContent = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $newContent);
        $newContent = trim(str_replace('"', "'", $newContent));
        libxml_use_internal_errors(true);
        $simpleXml = simplexml_load_string($newContent,null,LIBXML_COMPACT | LIBXML_NOCDATA | LIBXML_PARSEHUGE | LIBXML_NOEMPTYTAG);
        if($simpleXml !==  FALSE)
        {
          $body = $simpleXml;
          foreach($body->children() as $cat) {
            if(is_array($SelectedRootCategory) && !empty($SelectedRootCategory)) {
              if(count($SelectedRootCategory) > 1) { // array contain main category and sub category
                foreach ($SelectedRootCategory as $key => $value) {
                  $supplierCategory[] = $cat->$value ? $cat->$value : null;
                }
              }else { // array contain main category only
                $supplierCategory[] = $cat->$SelectedRootCategory[0] ? $cat->$SelectedRootCategory[0] : null;
              }
            }
          }
          $result = array();
          $data = [];
          $res = $this->xml2array($supplierCategory);
          foreach ($res as $sub) { // Loop outer array
            foreach ($sub as $val) { // Loop inner arrays
              $val = trim($val);
              if (!in_array($val, $result)) { // Check for duplicates
                $result[] = $val; // Add to result array
              }
            }
          }
          if($result) {
            if(!empty($categoryTranslation)) { // category translations exist
              foreach ($result as $key => $value) {
                $index = array_search($value,$categoryTranslation);
                if($index) {
                  $result[$key] = $value.'>>'.$index;
                }
              }
              $data['success'] = true;
              $data['translation'] = 1;
              $data['categoryDB'] = $this->getAllCategoriesTree();
              $data['data'] = array_filter($result);
            }else { // no category translations
              $data['success'] = true;
              $data['translation'] = 0;
              $data['categoryDB'] = $this->getAllCategoriesTree();
              $data['data'] = array_filter($result);
            }
          }else {
            $data['success'] = false;
            $data['data'] = null;
          }
          echo json_encode($data);
        }
      }
    }
  }


  protected function xml2array( $xmlObject, $out = array () )
  {
    foreach ( (array) $xmlObject as $index => $node )
    $out[$index] = ( is_object ( $node ) ) ? $this->xml2array ( $node ) : $node;
    return $out;
  }


  /**
   * Get All Attributes of product in database
   *
   * @access public
   * @return void
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function getAllAttributesAction() {
    //$attributes = Mage::getResourceModel('catalog/product_attribute_collection')->getItems();
    $attributes = Mage::getSingleton('catalog/convert_parser_product')
        ->getExternalAttributes();
    $attributes['id'] = 'id';
    $attributes['category'] = 'category';
    $attributes['brand'] = 'brand';
    $attributes['options'] = 'options';
    $attributes['options_img'] = 'options_img';
    $attributes['sub_category'] = 'sub_category';
    $attributes['image_gallery0'] = 'image_gallery0';
    $attributes['image_gallery1'] = 'image_gallery1';
    $attributes['image_gallery2'] = 'image_gallery2';
    $attributes['image_gallery3'] = 'image_gallery3';
    $attributes['image_gallery4'] = 'image_gallery4';
    $attributes['image_gallery5'] = 'image_gallery5';
    $attributes['image_gallery6'] = 'image_gallery6';
    $attributes['image_gallery7'] = 'image_gallery7';
    $attributes['image_gallery8'] = 'image_gallery8';
    $attributes['image_gallery9'] = 'image_gallery9';
    $attributes['code'] = 'code';
    $attributesArray = array();
    asort($attributes);
    foreach ($attributes as $_value => $_label){
        $attributesArray[] = array(
          'attribute_code'  => $_value,//$attribute->getAttributecode(),
          'attribute_label' => $_label,//$attribute->getFrontendLabel()
        );
    }
    return $attributesArray;
    //echo json_encode($attributesArray);
  }


  /**
   * Save Supplier Mapping Attribute
   *
   * @access public
   * @return void
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function saveMappingAttributesAction() {
    $request = $this->getRequest()->getParam('mappingArray');
    $mappingArray  = json_decode($this->getRequest()->getParam('mappingArray'),true);
    $supplierId  = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    if($supplier->getStatus()){
      $save = $supplier->setSupplierFormat($request)->save();
      $mapped = $supplier->setSupplierCheckIfMapped(1)->save();
      if($save && $mapped) {
        echo true;
      }else {
        echo false;
      }
    }
  }

  /**
   * Save Supplier Mapping Categories
   *
   * @access public
   * @return boolean
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function saveMappingCategoriesAction() {
    $request = $this->getRequest()->getParam('mappingCategoriesArray');
    $rootCategory = $this->getRequest()->getParam('rootCategory');
    $mappingArray  = json_decode($this->getRequest()->getParam('mappingCategoriesArray'),true);
    $rootCategoryArray = json_decode($this->getRequest()->getParam('rootCategory'),true);

    $supplierId  = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    if($supplier->getStatus()){
      $save = $supplier->setSupplierCategoryMapping($request)->save();
      $saveCategoryMapRoot = $this->saveCategoryMapRoot($supplier,$rootCategory);
      if($save && $saveCategoryMapRoot) {
        echo true;
      }else {
        echo false;
      }
    }
  }

  /**
   * Save Supplier Category and Sub Category Root
   *
   * @access public
   * @return boolean
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  protected function saveCategoryMapRoot($supplier,$rootCategory) {
    $saveCategoryRoot = $supplier->setSupplierMainCategoryRoot($rootCategory)->save();
    if($saveCategoryRoot) {
      return true;
    }else {
      return false;
    }
  }

  /**
   * Save Supplier Root
   *
   * @access public
   * @return void
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function saveRootAttributesAction() {
    $supplierRoot = $this->getRequest()->getParam('selectedRoot');
    $supplierId   = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    if($supplier->getStatus()){
      $save = $supplier->setSupplierXmlRoot($supplierRoot)->save();
      if($save) {
        echo true;
      }else {
        echo false;
      }
    }
  }

  /**
   * Save Supplier Translation
   *
   * @access public
   * @return void
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function saveSupplierTranslationsAction() {
    $request = $this->getRequest()->getPost('translationArray');
    $translationArray  = json_decode($this->getRequest()->getPost('translationArray'),true);
    $supplierId  = (int) $this->getRequest()->getPost('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    $supplierTranslations = $supplier->getSupplierXmlTranslations();
    if($supplier->getStatus()){
      $save = $supplier->setSupplierXmlTranslations($request)->save();
      if($save) {
        echo true;
      }else {
        echo false;
      }
    }
  }

  /**
   * Save Supplier Category Translation
   *
   * @access public
   * @return void
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function saveSupplierCatTranslationsAction() {
    $request = $this->getRequest()->getPost('translationCatArray');
    $translationArray  = json_decode($this->getRequest()->getPost('translationCatArray'),true);
    $supplierId  = (int) $this->getRequest()->getPost('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    if($supplier->getStatus()){
      $save = $supplier->setSupplierXmlCatTranslations($request)->save();
      if($save) {
        echo true;
      }else {
        echo false;
      }
    }
  }

  /**
   * Check Supplier Translations
   *
   * @access public
   * @return void
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function checkSupplierTranslationsAction() {
    $supplierId  = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    $supplierTranslations = $supplier->getSupplierXmlTranslations();
    $results = [];
    if($supplier->getStatus()){
      if(!empty($supplierTranslations)) {
        $results['saved'] = true;
        $results['data'] = json_decode($supplierTranslations);
      }else {
        $results['saved'] = false;
      }
      echo json_encode($results);
    }
  }

  /**
   * Check Supplier Category Translations
   *
   * @access public
   * @return void
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function checkSupplierCatTranslationsAction() {
    $supplierId  = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    $supplierTranslations = $supplier->getSupplierXmlCatTranslations();
    $results = [];
    if($supplier->getStatus()){
      if(!empty($supplierTranslations)) {
        $results['saved'] = true;
        $results['data'] = json_decode($supplierTranslations);
      }else {
        $results['saved'] = false;
      }
      echo json_encode($results);
    }
  }


  /**
   * Save XML Root
   *
   * @access public
   * @return void
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function saveXMLRootAttributesAction() {
    $XMLRoot = $this->getRequest()->getParam('selectedRoot');
    $supplierId   = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    if($supplier->getStatus()){
      $save = $supplier->setSupplierXmlRootField($XMLRoot)->save();
      if($save) {
        echo true;
      }else {
        echo false;
      }
    }
  }

  /**
   * Get Supplier Category Translations
   *
   * @access public
   * @return array
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function getAllCategoriesTranslations($supplierId) {
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    $supplierTranslations = (array) json_decode($supplier->getSupplierXmlCatTranslations());
    $results = [];
    if($supplier->getStatus()){
      if(!empty($supplierTranslations)) {
        return $supplierTranslations;
      }
    }
  }

  /**
   * Get All Categories Tree
   *
   * @access public
   * @return array
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function getAllCategoriesTree() {
    $parentCategoryId = 3;
    $cat = Mage::getModel('catalog/category')->load($parentCategoryId);
    $subcats = $cat->getChildren();
    $categoriesArray = array();

    foreach(explode(',',$subcats) as $subCatid)
    {
      $_category = Mage::getModel('catalog/category')->load($subCatid);
      if($_category->getIsActive()) {
        $categoriesArray[] = $_category->getName();
        $sub_cat = Mage::getModel('catalog/category')->load($_category->getId());
        $sub_subcats = $sub_cat->getChildren();
        foreach(explode(',',$sub_subcats) as $sub_subCatid)
        {
          $_sub_category = Mage::getModel('catalog/category')->load($sub_subCatid);
          if($_sub_category->getIsActive()) {
              $categoriesArray[] = $_category->getName().'=>'.$_sub_category->getName();
              $sub_sub_cat = Mage::getModel('catalog/category')->load($sub_subCatid);
              $sub_sub_subcats = $sub_sub_cat->getChildren();
              foreach(explode(',',$sub_sub_subcats) as $sub_sub_subCatid)
              {
                $_sub_sub_category = Mage::getModel('catalog/category')->load($sub_sub_subCatid);
                if($_sub_sub_category->getIsActive()) {
                  $categoriesArray[] = $_category->getName().'=>'.$_sub_category->getName().'=>'.$_sub_sub_category->getName();
                }
              }
           }
         }
      }
    }
    return $categoriesArray;
  }

  public function FormatXMLAction() {
    // get supplier id
    $supplierId   = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    $result = [];
    if($supplier->getStatus()){
      // Supplier name
      $supplierName = $supplier->getSupplierName();
      // Get XML URL
      $xmlPath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'supplier/file'.$supplier->getSupplierXmlFile();
      $xmlURL = $supplier->getSupplierUrl();
      // Get XML Root
      $xmlRoot = '//'.$supplier->getSupplierXmlRootField();
      // Get XML Product Root
      $productRoot = $supplier->getSupplierXmlRoot();
      // Get Default weight
      $weight = '';//$supplier->getDefaultWeight();
      $defaultWeight = '';
      /*if($weight) {
        $defaultWeight = $weight;
      }else {
        $defaultWeight = 200;
      }*/
      // Get Time updated
      $this->getXmlLastModified($supplier,$xmlURL);
      // Get Min Product Qty in switchBlock
      $minQtyInStock = $supplier->getSupplierMinQtyOfProducts();//$supplier->getAttributeText('supplier_min_qty_of_products');
      // Get Mapping Array
      $format = $supplier->getSupplierFormat();
      $catMapping = $supplier->getSupplierCategoryMapping();
      $sizeMapp = $supplier->getSupplierSizeMapping();
      $colorMapp = $supplier->getSupplierColorMapping();
      $categoryTranslation = (array) json_decode($supplier->getSupplierXmlCatTranslations());
      $mapping = (array) json_decode($format);
      $categoryMapping = (array) json_decode($catMapping);
      if(!empty($sizeMapp) && !empty($colorMapp)) {
        $sizeMapping = (array) json_decode($sizeMapp);
        $colorMapping = (array) json_decode($colorMapp);
      }else if(!empty($sizeMapp)) {
        $sizeMapping = (array) json_decode($sizeMapp);
      }else if(!empty($colorMapp)) {
        $colorMapping = (array) json_decode($colorMapp);
      }else{
        $sizeMapping = '';
        $colorMapping = '';
      }
      $data = Mage::helper('codendot_picksel')->start($xmlPath,$xmlURL,$xmlRoot,$productRoot,$mapping,$categoryMapping,$sizeMapping,$colorMapping,$supplierName,$defaultWeight,$minQtyInStock,$categoryTranslation);
      if(!empty($data) && is_array($data)) {
        $result['success'] = true;
        $result['data'] = $data;
        Mage::log($this->debugFileName().' - Message: Data Generated successfully.', null, 'picksellers.log');
      }else {
        $result['success'] = false;
        $result['data'] = $data ? $data : $this->__('No Records Found!');
        Mage::log($this->debugFileName().' - Message: '.$data, null, 'picksellers.log');
      }
    }else {
      $result['success'] = false;
      $result['data'] = $this->__('Your supplier not activated yet!');
      Mage::log($this->debugFileName().' - Message: '.$data, null, 'picksellers.log');
    }
    echo json_encode($result);
    //echo json_encode($result,JSON_PRETTY_PRINT);
  }

  protected function getXmlLastModified($supplier,$xmlURL) {
    $newDate = '';
    $data = '';
    $size = '';
    $curl = curl_init($xmlURL);
    //don't fetch the actual page, you only want headers
    curl_setopt($curl, CURLOPT_NOBODY, true);
    //stop it from outputting stuff to stdout
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    // attempt to retrieve the modification date
    curl_setopt($curl, CURLOPT_FILETIME, true);
    curl_setopt($curl, CURLOPT_HEADER, true);
    $result = curl_exec($curl);
    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if ($result !== false) {
      if($statusCode == 200) {
        $timestamp = curl_getinfo($curl, CURLINFO_FILETIME);
        $size = curl_getinfo($curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        if ($timestamp != -1) { //otherwise unknown
          $newDate = date("Y-m-d H:i", $timestamp);
          $data['lastModified'] = $newDate;
          $data['filesize'] = $size;
          $timeModified = json_encode($data);
          if($timeModified) {
            $saveTime = $supplier->setSupplierXmlLastUpdatedTime($timeModified)->save();
          }
        }else { // if serve not returned filetime value
          $DateNow = date("Y-m-d H:i");
          $data['lastModified'] = $DateNow;
          $data['filesize'] = $size;
          $timeModified = json_encode($data);
          if($timeModified) {
            $saveTime = $supplier->setSupplierXmlLastUpdatedTime($timeModified)->save();
          }
        }
      }
    }
    curl_close($curl);
  }


  public function getSupplierDataSavedAction() {
    $data = [];
    $supplierId   = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    if($supplier->getStatus()){
      $supplierMapped = $supplier->getSupplierCheckIfMapped();
      if($supplierMapped) {
        $xmlRoot = $supplier->getSupplierXmlRootField();
        $productRoot = $supplier->getSupplierXmlRoot();
        $format = $supplier->getSupplierFormat();
        $mapping = (array) json_decode($format);
        $data['checked'] = true;
        $data['xmlRoot'] = $xmlRoot;
        $data['productRoot'] = $productRoot;
        $data['mapping'] = $mapping;
      }else {
        $data['checked'] = false;
      }
      echo json_encode($data);
    }
  }

  public function getSupplierCategoryMappingSavedAction() {
    $data = [];
    $supplierId   = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    if($supplier->getStatus()){
      $categoryMapping = $supplier->getSupplierCategoryMapping();
      $rootCategoryMapping = $supplier->getSupplierMainCategoryRoot();
      if(!empty($categoryMapping)) {
        $mapping = (array) json_decode($categoryMapping);
        $rootCat = (array) json_decode($rootCategoryMapping);
        $data['checked'] = true;
        $data['mapping'] = $mapping;
        $data['rootCategoryMapped'] = $rootCat;
      }else {
        $data['checked'] = false;
      }
      echo json_encode($data);
    }
  }

  public function checkXmlUpdateAction() {
    $supplierId   = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    $result = '';
    if($supplier->getStatus()){

      $xmlURL = $supplier->getSupplierUrl();
      $supplierTimeUpdated = $supplier->getSupplierXmlLastUpdatedTime();
      $data = (array) json_decode($supplierTimeUpdated);
      $curl = curl_init($xmlURL);
      //don't fetch the actual page, you only want headers
      curl_setopt($curl, CURLOPT_NOBODY, true);
      //stop it from outputting stuff to stdout
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      // attempt to retrieve the modification date
      curl_setopt($curl, CURLOPT_FILETIME, true);
      curl_setopt($curl, CURLOPT_HEADER, true);
      $result = curl_exec($curl);
      $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      if ($result !== false) {
        if($statusCode == 200) {
          $timestamp = curl_getinfo($curl, CURLINFO_FILETIME);
          $newFileSize = curl_getinfo($curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
          if ($timestamp != -1) { //otherwise unknown
            $newDate = date("Y-m-d H:i", $timestamp);
            $lastModified = $data['lastModified'];
            $fileSize = $data['filesize'];
            if((strtotime($lastModified) < strtotime($newDate)) && ($filesize < $newFileSize)) {
              echo true;
            }else {
              echo false;
            }
          }else { // if serve not returned filetime value
            $newDate = date("Y-m-d H:i");
            $lastModified = $data['lastModified'];
            $fileSize = $data['filesize'];
            if((strtotime($lastModified) < strtotime($newDate)) && ($filesize < $newFileSize)) {
              echo true;
            }else {
              echo false;
            }
          }
        }
      }
      curl_close($curl);
    }
  }

  public function checkSupplierFirstRunAction() {
    $supplierId   = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    $result = '';
    if($supplier->getStatus()){
      $lastUpdatedTime = $supplier->getSupplierXmlLastUpdatedTime();
      if(!empty($lastUpdatedTime)) {
        $result['success'] = true;
      }else {
        $result['success'] = false;
      }
      echo json_encode($result);
    }
  }

  protected function curl_get_file_contents($xmlPath) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_URL, $xmlPath);
    // attempt to retrieve the modification date
    curl_setopt($curl, CURLOPT_FILETIME, true);
    $contents = curl_exec($curl);
    if ($contents === false) {
        die (curl_error($curl));
    }
    curl_close($curl);
    return $contents;
  }

  // Size Mapping Functions

  /**
   * Get Selected Supplier Node Value
   *
   * @access public
   * @return json
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
   public function getSizeValueMappingByNodeAction() {
     $supplierNode = array();
     $supplierId  = (int) $this->getRequest()->getParam('supplierId');
     $node  = $this->getRequest()->getParam('node');
     $sizesNode = explode('->',$node);
     $countNode = count($sizesNode);
     $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);

     if($supplier->getStatus()){
       //$xmlPath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'supplier/file'.$supplier->getSupplierXmlFile();
       $xmlURL = $supplier->getSupplierUrl();
       $mapping = $supplier->getSupplierFormat();
       $mapArray = (array) json_decode($mapping);
       if(array_key_exists('options', $mapArray)) { // Explode color from option value
         $contents = $this->curl_get_file_contents($xmlURL);
         if ($contents) {
           $xmlString = $contents;
           $newContent = str_replace(array("\n", "\r", "\t", "<br>","<b>"), '', $xmlString);
           $newContent = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $newContent);
           $newContent = trim(str_replace('"', "'", $newContent));
           libxml_use_internal_errors(true);
           $simpleXml = simplexml_load_string($newContent,null,LIBXML_COMPACT | LIBXML_NOCDATA | LIBXML_PARSEHUGE | LIBXML_NOEMPTYTAG);
           if($simpleXml !==  FALSE)
           {
             $body = $simpleXml;
             foreach($body->children() as $nod) {
               if(!empty($sizesNode) && is_array($sizesNode)) {
                 switch ($countNode) {
                   case 1:
                     $supplierNode[] = $nod->$sizesNode[0] ? $nod->$sizesNode[0] : null;
                     break;
                   case 2:
                     if(!empty($nod->$sizesNode[0]) && is_array((array)$nod->$sizesNode[0])) {
                       foreach ($nod->$sizesNode[0] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[1]) ? trim($value->$sizesNode[1]) : null;
                       }
                     }
                     break;
                   case 3:
                     if(!empty($nod->$sizesNode[0]->$sizesNode[1]) && is_array((array)$nod->$sizesNode[0]->$sizesNode[1])) {
                       foreach ($nod->$sizesNode[0]->$sizesNode[1] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[2]) ? trim($value->$sizesNode[2]) : null;
                       }
                     }
                     break;
                   case 4:
                     if(!empty($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]) && is_array((array)$nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2])) {
                       foreach ($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[3]) ? trim($value->$sizesNode[3]) : null;
                       }
                     }
                     break;
                   case 5:
                     if(!empty($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]->$sizesNode[3]) && is_array((array)$nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]->$sizesNode[3])) {
                       foreach ($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]->$sizesNode[3] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[4]) ? trim($value->$sizesNode[4]) : null;
                       }
                     }
                     break;
                   default:
                     break;
                 }
               }
             }
             $result = array();
             $data = [];
             $res = json_decode(json_encode($supplierNode), true);
             $array_reduce = array_reduce($res, 'array_merge', array());
             if($array_reduce === null) {
               $resu = $res;
             }else {
               $resu = array_reduce($res, 'array_merge', array());
             }
             $array_unique = array_unique($resu);
             $result = $array_unique;
             $result = array_values($result);
             sort($result);
             foreach ($result as $key => $value) {
               $_prodOption = $value;
               $_prodOptionStr = substr($_prodOption, strpos($_prodOption, ":") + 1);
               $arr = explode(":", $_prodOptionStr, 2);
               $first = $arr[0];
               $charaCount = substr_count($first, '-');
               if($charaCount == 2) { // '-' exist two time
                 $_prodOptionBeforeSep = substr($first, 0, strpos($first, '-', strpos($first, '-')+1));
                 $lastString = substr(strstr(substr(strstr($first, "-"),1),"-"),1);
                 $_prodSize = $lastString;
                 $result[$key] = $_prodSize;
               }else if($charaCount == 1) { // '-' exist one time
                 $_prodOptionStr = substr($_prodOption, strpos($_prodOption, ":") + 1);
                 $arr = explode(":", $_prodOptionStr, 2);
                 $first = $arr[0];
                 $_prodOptionAfterSep = explode("-", $first, 2);
                 $_prodSize = $_prodOptionAfterSep[1];
                 $result[$key] = $_prodSize;
               }
             }
             $result = array_unique($result);
             if(!empty($result) && is_array($result)) {
               $data['success'] = true;
               $data['data'] = array_filter($result);
               $data['sizes'] = $this->getAllSizes();
             }else {
               $data['success'] = false;
               $data['data'] = null;
               $data['sizes'] =null;
             }
             echo json_encode($data);
           }
         }
       }else {// if color not in concatenation value
         $contents = $this->curl_get_file_contents($xmlURL);
         if ($contents) {
           $xmlString = $contents;
           $newContent = str_replace(array("\n", "\r", "\t", "<br>","<b>"), '', $xmlString);
           $newContent = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $newContent);
           $newContent = trim(str_replace('"', "'", $newContent));
           libxml_use_internal_errors(true);
           $simpleXml = simplexml_load_string($newContent,null,LIBXML_COMPACT | LIBXML_NOCDATA | LIBXML_PARSEHUGE | LIBXML_NOEMPTYTAG);
           if($simpleXml !==  FALSE)
           {
             $body = $simpleXml;
             foreach($body->children() as $nod) {
               if(!empty($sizesNode) && is_array($sizesNode)) {
                 switch ($countNode) {
                   case 1:
                     $supplierNode[] = $nod->$sizesNode[0] ? $nod->$sizesNode[0] : null;
                     break;
                   case 2:
                     if(!empty($nod->$sizesNode[0]) && is_array((array)$nod->$sizesNode[0])) {
                       foreach ($nod->$sizesNode[0] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[1]) ? trim($value->$sizesNode[1]) : null;
                       }
                     }
                     break;
                   case 3:
                     if(!empty($nod->$sizesNode[0]->$sizesNode[1]) && is_array((array)$nod->$sizesNode[0]->$sizesNode[1])) {
                       foreach ($nod->$sizesNode[0]->$sizesNode[1] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[2]) ? trim($value->$sizesNode[2]) : null;
                       }
                     }
                     break;
                   case 4:
                     if(!empty($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]) && is_array((array)$nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2])) {
                       foreach ($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[3]) ? trim($value->$sizesNode[3]) : null;
                       }
                     }
                     break;
                   case 5:
                     if(!empty($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]->$sizesNode[3]) && is_array((array)$nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]->$sizesNode[3])) {
                       foreach ($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]->$sizesNode[3] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[4]) ? trim($value->$sizesNode[4]) : null;
                       }
                     }
                     break;
                   default:
                     break;
                 }
               }
             }
             $result = array();
             $data = [];
             $res = json_decode(json_encode($supplierNode), true);
             $array_reduce = array_reduce($res, 'array_merge', array());
             if($array_reduce === null) {
               $resu = $res;
             }else {
               $resu = array_reduce($res, 'array_merge', array());
             }
             $array_unique = array_unique($resu);
             $result = $array_unique;
             $result = array_values($result);
             sort($result);
             if(!empty($result) && is_array($result)) {
               $data['success'] = true;
               $data['data'] = array_filter($result);
               $data['sizes'] = $this->getAllSizes();
             }else {
               $data['success'] = false;
               $data['data'] = null;
               $data['sizes'] =null;
             }
             echo json_encode($data);
           }
         }
       }
     }
   }


  /**
   * Get All Sizes in Magento
   *
   * @access public
   * @return array
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */

  protected function getAllSizes(){
    $attributeCode = "size";
    $arrayCode = array();
    $attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeCode);
    $options = Mage::getModel('eav/entity_attribute_source_table')
             ->setAttribute($attribute)
             ->getAllOptions(false);

    foreach ($options as $option) {
      $arrayCode[] = $option['label'];
    }
    if(!empty($arrayCode) && is_array($arrayCode)) {
      asort($arrayCode);
      $arrayCode = array_map('strtolower', $arrayCode);
      $arrayCode = array_values($arrayCode);
      return $arrayCode;
    }
  }

  /**
   * Save Supplier Size Mapping
   *
   * @access public
   * @return Boolean
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function saveMappingSizesAction() {
    $request = $this->getRequest()->getParam('mappingSizesArray');
    $rootSize = $this->getRequest()->getParam('rootSize');
    $mappingArray  = json_decode($this->getRequest()->getParam('mappingSizesArray'),true);

    $supplierId  = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    if($supplier->getStatus()){
      $save = $supplier->setSupplierSizeMapping($request)->save();
      $saveRootSize = $supplier->setSupplierSizeMappingRoot($rootSize)->save();
      if($save && $saveRootSize) {
        echo true;
      }else {
        echo false;
      }
    }
  }

  /**
   * Get Saved Supplier Size Mapping
   *
   * @access public
   * @return array
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function getSupplierSizeMappingSavedAction() {
    $data = [];
    $supplierId   = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    if($supplier->getStatus()){
      $sizeMapping = $supplier->getSupplierSizeMapping();
      $rootSizeMapping = $supplier->getSupplierSizeMappingRoot();
      if(!empty($sizeMapping)) {
        $mapping = (array) json_decode($sizeMapping);
        $rootSize = $rootSizeMapping;
        $data['checked'] = true;
        $data['mapping'] = $mapping;
        $data['rootSizeMapped'] = $rootSize;
      }else {
        $data['checked'] = false;
      }
      echo json_encode($data);
    }
  }

  // Color Mapping Functions

  /**
   * Get Selected Supplier Node Value
   *
   * @access public
   * @return json
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
   public function getColorValueMappingByNodeAction() {
     $supplierNode = array();
     $supplierId  = (int) $this->getRequest()->getParam('supplierId');
     $node  = $this->getRequest()->getParam('node');
     $sizesNode = explode('->',$node);
     $countNode = count($sizesNode);
     $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);

     if($supplier->getStatus()){
       //$xmlPath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'supplier/file'.$supplier->getSupplierXmlFile();
       $xmlURL = $supplier->getSupplierUrl();
       $mapping = $supplier->getSupplierFormat();
       $mapArray = (array) json_decode($mapping);
       if(array_key_exists('options', $mapArray)) { // Explode color from option value
         $contents = $this->curl_get_file_contents($xmlURL);
         if ($contents) {
           $xmlString = $contents;
           $newContent = str_replace(array("\n", "\r", "\t", "<br>","<b>"), '', $xmlString);
           $newContent = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $newContent);
           $newContent = trim(str_replace('"', "'", $newContent));
           libxml_use_internal_errors(true);
           $simpleXml = simplexml_load_string($newContent,null,LIBXML_COMPACT | LIBXML_NOCDATA | LIBXML_PARSEHUGE | LIBXML_NOEMPTYTAG);
           if($simpleXml !==  FALSE)
           {
             $body = $simpleXml;
             foreach($body->children() as $nod) {
               if(!empty($sizesNode) && is_array($sizesNode)) {
                 switch ($countNode) {
                   case 1:
                     $supplierNode[] = $nod->$sizesNode[0] ? $nod->$sizesNode[0] : null;
                     break;
                   case 2:
                     if(!empty($nod->$sizesNode[0]) && is_array((array)$nod->$sizesNode[0])) {
                       foreach ($nod->$sizesNode[0] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[1]) ? trim($value->$sizesNode[1]) : null;
                       }
                     }
                     break;
                   case 3:
                     if(!empty($nod->$sizesNode[0]->$sizesNode[1]) && is_array((array)$nod->$sizesNode[0]->$sizesNode[1])) {
                       foreach ($nod->$sizesNode[0]->$sizesNode[1] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[2]) ? trim($value->$sizesNode[2]) : null;
                       }
                     }
                     break;
                   case 4:
                     if(!empty($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]) && is_array((array)$nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2])) {
                       foreach ($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[3]) ? trim($value->$sizesNode[3]) : null;
                       }
                     }
                     break;
                   case 5:
                     if(!empty($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]->$sizesNode[3]) && is_array((array)$nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]->$sizesNode[3])) {
                       foreach ($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]->$sizesNode[3] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[4]) ? trim($value->$sizesNode[4]) : null;
                       }
                     }
                     break;
                   default:
                     break;
                 }
               }
             }
             $result = array();
             $data = [];
             $res = json_decode(json_encode($supplierNode), true);
             $array_reduce = array_reduce($res, 'array_merge', array());
             if($array_reduce === null) {
               $resu = $res;
             }else {
               $resu = array_reduce($res, 'array_merge', array());
             }
             $array_unique = array_unique($resu);
             $result = $array_unique;
             $result = array_values($result);
             sort($result);
             foreach ($result as $key => $value) {
               $_prodOption = $value;
               $_prodOptionStr = substr($_prodOption, strpos($_prodOption, ":") + 1);
               $arr = explode(":", $_prodOptionStr, 2);
               $first = $arr[0];
               $charaCount = substr_count($first, '-');
               if($charaCount == 2) { // '-' exist two time
                 $_prodOptionBeforeSep = substr($first, 0, strpos($first, '-', strpos($first, '-')+1));
                 $lastString = substr(strstr(substr(strstr($first, "-"),1),"-"),1);
                 $_prodColor = $_prodOptionBeforeSep;
                 $result[$key] = $_prodColor;
               }else if($charaCount == 1) { // '-' exist one time
                 $_prodOptionStr = substr($_prodOption, strpos($_prodOption, ":") + 1);
                 $arr = explode(":", $_prodOptionStr, 2);
                 $first = $arr[0];
                 $_prodOptionAfterSep = explode("-", $first, 2);
                 $_prodColor = $_prodOptionAfterSep[0];
                 $result[$key] = $_prodColor;
               }
             }
             $result = array_unique($result);
             if(!empty($result) && is_array($result)) {
               $data['success'] = true;
               $data['data'] = array_filter($result);
               $data['colors'] = $this->getAllColors();
             }else {
               $data['success'] = false;
               $data['data'] = null;
               $data['colors'] =null;
             }
             echo json_encode($data);
           }
         }
       }else { // if color not in concatenation value
         $contents = $this->curl_get_file_contents($xmlURL);
         if ($contents) {
           $xmlString = $contents;
           $newContent = str_replace(array("\n", "\r", "\t", "<br>","<b>"), '', $xmlString);
           $newContent = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $newContent);
           $newContent = trim(str_replace('"', "'", $newContent));
           libxml_use_internal_errors(true);
           $simpleXml = simplexml_load_string($newContent,null,LIBXML_COMPACT | LIBXML_NOCDATA | LIBXML_PARSEHUGE | LIBXML_NOEMPTYTAG);
           if($simpleXml !==  FALSE)
           {
             $body = $simpleXml;
             foreach($body->children() as $nod) {
               if(!empty($sizesNode) && is_array($sizesNode)) {
                 switch ($countNode) {
                   case 1:
                     $supplierNode[] = $nod->$sizesNode[0] ? $nod->$sizesNode[0] : null;
                     break;
                   case 2:
                     if(!empty($nod->$sizesNode[0]) && is_array((array)$nod->$sizesNode[0])) {
                       foreach ($nod->$sizesNode[0] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[1]) ? trim($value->$sizesNode[1]) : null;
                       }
                     }
                     break;
                   case 3:
                     if(!empty($nod->$sizesNode[0]->$sizesNode[1]) && is_array((array)$nod->$sizesNode[0]->$sizesNode[1])) {
                       foreach ($nod->$sizesNode[0]->$sizesNode[1] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[2]) ? trim($value->$sizesNode[2]) : null;
                       }
                     }
                     break;
                   case 4:
                     if(!empty($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]) && is_array((array)$nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2])) {
                       foreach ($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[3]) ? trim($value->$sizesNode[3]) : null;
                       }
                     }
                     break;
                   case 5:
                     if(!empty($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]->$sizesNode[3]) && is_array((array)$nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]->$sizesNode[3])) {
                       foreach ($nod->$sizesNode[0]->$sizesNode[1]->$sizesNode[2]->$sizesNode[3] as $key => $value) {
                         $supplierNode[] = trim($value->$sizesNode[4]) ? trim($value->$sizesNode[4]) : null;
                       }
                     }
                     break;
                   default:
                     break;
                 }
               }
             }
             $result = array();
             $data = [];
             $res = json_decode(json_encode($supplierNode), true);
             $array_reduce = array_reduce($res, 'array_merge', array());
             if($array_reduce === null) {
               $resu = $res;
             }else {
               $resu = array_reduce($res, 'array_merge', array());
             }
             $array_unique = array_unique($resu);
             $result = $array_unique;
             $result = array_values($result);
             sort($result);
             if(!empty($result) && is_array($result)) {
               $data['success'] = true;
               $data['data'] = array_filter($result);
               $data['colors'] = $this->getAllColors();
             }else {
               $data['success'] = false;
               $data['data'] = null;
               $data['colors'] =null;
             }
             echo json_encode($data);
           }
         }
       }

     }
   }

  /**
   * Get All Colors in Magento
   *
   * @access public
   * @return array
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */

  protected function getAllColors(){
    $attributeCode = "color";
    $arrayCode = array();
    $attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeCode);
    $options = Mage::getModel('eav/entity_attribute_source_table')
             ->setAttribute($attribute)
             ->getAllOptions(false);

    foreach ($options as $option) {
      $arrayCode[] = $option['label'];
    }
    if(!empty($arrayCode) && is_array($arrayCode)) {
      asort($arrayCode);
      $arrayCode = array_map('strtolower', $arrayCode);
      $arrayCode = array_values($arrayCode);
      return $arrayCode;
    }
  }

  /**
   * Save Supplier Colors Mapping
   *
   * @access public
   * @return Boolean
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function saveMappingColorsAction() {
    $request = $this->getRequest()->getParam('mappingColorsArray');
    $rootColor = $this->getRequest()->getParam('rootColor');
    $mappingArray  = json_decode($this->getRequest()->getParam('mappingColorsArray'),true);

    $supplierId  = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    if($supplier->getStatus()){
      $save = $supplier->setSupplierColorMapping($request)->save();
      $saveRootColor = $supplier->setSupplierColorMappingRoot($rootColor)->save();
      if($save && $saveRootColor) {
        echo true;
      }else {
        echo false;
      }
    }
  }

  /**
   * Get Saved Supplier Colors Mapping
   *
   * @access public
   * @return array
   * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
   */
  public function getSupplierColorMappingSavedAction() {
    $data = [];
    $supplierId   = (int) $this->getRequest()->getParam('supplierId');
    $supplier = Mage::getModel('codendot_picksel/supplier')->load($supplierId);
    if($supplier->getStatus()){
      $colorMapping = $supplier->getSupplierColorMapping();
      $rootColorMapping = $supplier->getSupplierColorMappingRoot();
      if(!empty($colorMapping)) {
        $mapping = (array) json_decode($colorMapping);
        $rootColor = $rootColorMapping;
        $data['checked'] = true;
        $data['mapping'] = $mapping;
        $data['rootColorMapped'] = $rootColor;
      }else {
        $data['checked'] = false;
      }
      echo json_encode($data);
    }
  }


}
