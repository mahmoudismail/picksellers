<?php/** * CodenDot_PickSel extension * * NOTICE OF LICENSE * * This source file is subject to the MIT License * that is bundled with this package in the file LICENSE.txt. * It is also available through the world-wide-web at this URL: * http://opensource.org/licenses/mit-license.php * * @category       CodenDot * @package        CodenDot_PickSel * @copyright      Copyright (c) 2017 * @license        http://opensource.org/licenses/mit-license.php MIT License *//** * PickSel default helper * * @category    CodenDot * @package     CodenDot_PickSel * @author      Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com */class CodenDot_PickSel_Helper_Data extends Mage_Core_Helper_Abstract{    protected $debug = false;    /**     * convert array to options     *     * @access public     * @param $options     * @return array     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com     */    public function convertOptions($options)    {        $converted = array();        foreach ($options as $option) {            if (isset($option['value']) && !is_array($option['value']) &&                isset($option['label']) && !is_array($option['label'])) {                $converted[$option['value']] = $option['label'];            }        }        return $converted;    }    /**     * Trace Debug     *     * @access public     * @param $message, $data     * @return Void     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com     */    public function trace($message,$data = null)    {        $bt = debug_backtrace();        $caller = array_shift($bt);        $line = $caller['line'];        $file = $caller['file'];        $class = $caller['class'];        if ($this->debug) {          $result = json_decode($data);          if ($message !== 'json') {            echo '<p>'.$message.' <small style="color:#aaa; font-size:11px; display:inline-block; padding:0 25px;">'.$class.' at '.$line.' in '.$file.'</small></p>';            if($data != null) {                echo((is_array($data) || is_object($data)) ? '<pre>' . print_r($data, true) . '</pre>' : $data);            }            echo '<hr>';          }else {            echo '<p>'.$message.' <small style="color:#aaa; font-size:11px; display:inline-block; padding:0 25px;">'.$class.' at '.$line.' in '.$file.'</small></p>';            if($data != null) {                echo('<pre>' . json_encode($data,JSON_PRETTY_PRINT) . '</pre>');            }            echo '<hr>';          }        }    }    /**     * Debug Function class name for log file     *     * @access public     * @return string     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com     */    protected function debugFileName() {      $bt = debug_backtrace();      $caller = array_shift($bt);      $line = $caller['line'];      $file = $caller['file'];      $class = $caller['class'];      $data = $class.' at '.$line.' in '.$file;      return $data;    }    /**     * Start Parse XML     *     * @access public     * @param $xmlPath,$xmlRoot,$productRoot,$mapping     * @return Products List     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com     */    public function start($xmlPath,$xmlURL,$xmlRoot,$productRoot,$mapping,$categoryMapping,$sizeMapping,$colorMapping,$supplierName,$defaultWeight,$minQtyInStock,$categoryTranslation)    {      $this->trace('Display Map Data', $mapping);      $this->trace('Get Xml Data');      $data = $this->getXmlData($xmlURL,$xmlRoot);      $this->trace('Display Xml Data', $data);      if(!empty($data) && is_array($data)){        $this->trace('Generate Products');        $products = $this->generateProducts($data,$mapping,$categoryMapping,$sizeMapping,$colorMapping,$productRoot,$supplierName,$defaultWeight,$minQtyInStock,$categoryTranslation);        $this->trace('Display Products List',$products);        $res = $products;        Mage::log($this->debugFileName().' - Message: Generating Products and Displaying Products List', null, 'picksellers.log');      }else {        $res= $data;        Mage::log($this->debugFileName().' - Message: '.$data.' Empty Data, its not an array', null, 'picksellers.log');      }      return $res;    }    /**     * Get XML Data     *     * @access protected     * @param $xmlPath,$xmlRoot,$mapping     * @return array     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com     */     protected function getXmlData($xmlPath,$xmlRoot,$mapping){      $contents = $this->curl_get_file_contents($xmlPath);      if (!empty($contents)) {        $xmlString = $contents;        $newContent = str_replace(array("\n", "\r", "\t", "<br>","<b>"), '', $xmlString);        $newContent = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $newContent);        $newContent = trim(str_replace('"', "'", $newContent));        libxml_use_internal_errors(true);        $simpleXml = simplexml_load_string($newContent,null,LIBXML_COMPACT | LIBXML_NOCDATA | LIBXML_PARSEHUGE | LIBXML_NOEMPTYTAG);        if($simpleXml !==  FALSE)        {          $body = $simpleXml;          $xmlResult = json_decode(json_encode((array)$body),true);          Mage::log($this->debugFileName().' - Message: Getting Xml data successfully', null, 'picksellers.log');        }else {          foreach(libxml_get_errors() as $error) {            Mage::log($this->debugFileName().' - Failed loading XML Error Message: '.$error->message, null, 'picksellers.log');          }          $xmlResult = $this->__('Failed loading XML for some reason.');          Mage::log($this->debugFileName().' - Message: '.$xmlResult['data'], null, 'picksellers.log');        }      }else {        $xmlResult = $this->__('Empty Content.');        Mage::log($this->debugFileName().' - Message: '.$xmlResult['data'], null, 'picksellers.log');      }       return $xmlResult;     }    /**     * Generate Products from XML Data     *     * @access protected     * @param $data,$mapping     * @return $list     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com     */    protected function generateProducts($data,$mapping,$categoryMapping,$sizeMapping,$colorMapping,$productRoot,$supplierName,$defaultWeight,$minQtyInStock,$categoryTranslation){        $list = array();        foreach($data[$productRoot] as $i => $child){            $list[$i] = array();            $this->trace('Initial new product item in position '.$i);            $this->trace('Data convertable is',$child);            foreach ($child as $attr => $value) {                if(!empty($value) && is_array($value)) {                    $this->trace('The value of '.$attr.' at position '.$i.' is array');                    $pathes = $this->build($value,$attr.'.');                    foreach ($pathes as $j => $pathItem) {                        if(!empty($pathItem) && is_array($pathItem)){                            foreach($pathItem as $path => $resultValue){                                $index = array_search($path,$mapping);                                if($index !== false) {                                    if(!isset($list[$i][$index])){                                        $list[$i][$index] = [];                                    }                                    $list[$i][$index][] = $resultValue;                                }                            }                        }                    }                }else{                    $this->trace('The value of '.$attr.' at position '.$i.' is string');                    $index = array_search($attr,$mapping);                    if($index !== false) {                        //$this->trace('The index of '.$attr.' is '.$index);                        $list[$i][$index] = $value;                    }                }            }        }        $dataProducts = [];        $simpleSkus = [];        $productsResults = '';        $categoryOfProd = '';        $subCategoryOfProd = '';        $mediaGalleyArray = [];        $productNameMaxLength = 100;        $newCatMap = [];        if(!empty($list) && is_array($list)){
            foreach($list as $i => $product){
              $prodCat  = $product['category'] ? $product['category'] : '';
              foreach ($categoryMapping as $key => $value) {
                $newVal = explode(",",$value);
                if(in_array($prodCat,$newVal)){
                  $str = $key;
                  $replaceComma = str_replace(",",";;",$str);
                  $cleanVal = str_replace("=>","/",$replaceComma);
                  $categoryOfProd = $cleanVal;
                }
              }
              //$this->trace('$categoryOfProd >>>>>>>> ',$categoryOfProd);
              $prodSubCat  = $product['sub_category'] ? $product['sub_category'] : '';
              if(!empty($prodSubCat)) {
                $subCat = array_search(trim($prodSubCat),$categoryMapping);
                if ($subCat !== false) {
                  $replaceComma = str_replace(",",";;",$subCat);
                  $cleanVal = str_replace("=>","/",$replaceComma);
                  $subCategoryOfProd = ';;'.$cleanVal;
                }
                foreach ($categoryMapping as $key => $value) {
                  $newVal = explode(",",$value);
                  if(in_array($prodSubCat,$newVal)){
                    $str = $key;
                    $replaceComma = str_replace(",",";;",$str);
                    $cleanVal = str_replace("=>","/",$replaceComma);
                    $subCategoryOfProd = ';;'.$cleanVal;
                  }
                }
              }
              if(empty($categoryOfProd) && !empty($subCategoryOfProd)){
                $subCategoryOfProd = str_replace(";;","",$subCategoryOfProd);
              }

              $mediaGalleyArray = [];
              for ($mg=0; $mg < 10; $mg++) { // mg: media gallery , one by one
                if(!empty($product['image_gallery'.$mg])) {
                  $mediaGalleyArray['image_gallery'.$mg] = '+'.trim($product['image_gallery'.$mg]);
                }else { // options images
                  if(!empty($product['options_img'][$mg])) { // list of images
                    $mediaGalleyArray['options_img'.$mg] = '+'.trim($product['options_img'][$mg]);
                  }
                }
              }

              //$this->trace('************************* Products',$product);
              //$this->trace('************************* Media Gallery',$mediaGalleyArray);
                if(isset($product['color']) || isset($product['size']) || isset($product['options'])){ // configurable product with size or color or options
                  $this->trace('configurable products .....');
                    if(isset($product['size']) && is_array($product['size'])) { // generate simple product with array of attribute value
                      $this->trace('configurable attributes is array..');
                      $k = 0 ;
                      foreach ($product['size'] as $j => $valAttr) {
                        $quantityOfPrd = isset($product['qty'][$j]) ? $product['qty'][$j] : (int) $minQtyInStock;
                        if($quantityOfPrd >= (int) $minQtyInStock) { // if qty of simple product > 5
                          // Size Mapping
                          $prodSize = '';
                          if(!empty($sizeMapping) && is_array($sizeMapping)) {
                            $sizeVal = trim($product['size'][$j]) ? trim($product['size'][$j]) : '';
                            $sizeMap = array_search($sizeVal,$sizeMapping);
                            if ($sizeMap !== false) { // size Mapping Exist in Array
                              $prodSize = $sizeMap;
                            }else { // size Mappingnot Exist in Array
                              $prodSize = trim($product['size'][$j]) ? trim($product['size'][$j]) : '';
                            }
                          }else { // size Mappingnot Exist in Array
                            $prodSize = trim($product['size'][$j]) ? trim($product['size'][$j]) : '';
                          }
                          // if($prodSize == 'Default') {
                          //   $prodSize = '';
                          // }
                          // Color Mapping
                          $prodColor = '';
                          if(!empty($colorMapping) && is_array($colorMapping)) {
                            $colorVal = $product['color'] ? $product['color'] : '';
                            $colorMap = array_search($colorVal,$colorMapping);
                            if ($colorMap !== false) { // size Mapping Exist in Array
                              $prodColor = $colorMap;
                            }else { // size Mappingnot Exist in Array
                              $prodColor = $product['color'] ? $product['color'] : '';
                            }
                          }else { // size Mappingnot Exist in Array
                            $prodColor = $product['color'] ? $product['color'] : '';
                          }
                          $dataProducts[$i][$j]['store'] = 'admin';
                          $dataProducts[$i][$j]['websites'] = 'base';
                          $dataProducts[$i][$j]['attribute_set'] = 'Default';
                          $dataProducts[$i][$j]['configurable_attributes'] = '';
                          $dataProducts[$i][$j]['type'] = 'simple';
                          $dataProducts[$i][$j]['supplier_prod_name'] = trim($product['name']) ? substr(trim($product['name']), 0, $productNameMaxLength) : 'no_name';
                          $dataProducts[$i][$j]['name'] = $supplierName.' '. array_search(trim($product['category']),$categoryTranslation). ' '.array_search(trim($product['sub_category']),$categoryTranslation).' '.strtolower($prodColor).' '.strtolower($prodSize);
                          $dataProducts[$i][$j]['system_name'] = $supplierName.' '. array_search(trim($product['category']),$categoryTranslation). ' '.array_search(trim($product['sub_category']),$categoryTranslation).' '.strtolower($prodColor).' '.strtolower($prodSize);
                          $dataProducts[$i][$j]['product_name'] = trim($product['name']) ? substr(trim($product['name']), 0, $productNameMaxLength) : 'no_name';
                          $dataProducts[$i][$j]['categories'] = $categoryOfProd.''.$subCategoryOfProd;
                          $dataProducts[$i][$j]['simples_skus'] = '';
                          $dataProducts[$i][$j]['sku'] = $product['sku'][$j] ? $product['sku'][$j] : trim(strtolower($product['id'] ? str_replace(' ','-',$product['id']) : ''). isset($prodColor) ? '-'.$prodColor : '' . isset($prodSize) ? '-'.$prodSize : '' );
                          $dataProducts[$i][$j]['code'] = trim(strtolower($product['code'] ? str_replace(' ','-',$product['code']) : ''));
                          $dataProducts[$i][$j]['prod_id'] = is_numeric(trim($product['id'])) ? trim(strtolower($product['id'] ? $product['id'] : '')) : '';
                          $dataProducts[$i][$j]['has_options'] = '0'; // simple product has_option default 0
                          $dataProducts[$i][$j]['news_from_date'] = '';
                          $dataProducts[$i][$j]['news_to_date'] = '';
                          $dataProducts[$i][$j]['weight'] = $defaultWeight; // Default 200 g ; Unit in gramme
                          $dataProducts[$i][$j]['description'] = '';//$product['description'];
                          $dataProducts[$i][$j]['short_description'] = '';//$product['description'];
                          $dataProducts[$i][$j]['meta_title'] = '';
                          $dataProducts[$i][$j]['meta_keyword'] = '';
                          $dataProducts[$i][$j]['meta_description'] = '';
                          $dataProducts[$i][$j]['url_key'] = '';
                          $dataProducts[$i][$j]['url_path'] = '';
                          $dataProducts[$i][$j]['image'] = '';//$product['image'] ? '+'.$product['image'] : 'no_image';
                          $dataProducts[$i][$j]['media_gallery'] = '';//implode(';',$mediaGalleyArray);
                          $dataProducts[$i][$j]['small_image'] = '';//$product['image'] ? '+'.$product['image'] : 'no_image';
                          $dataProducts[$i][$j]['thumbnail'] = '';//$product['image'] ? '+'.$product['image'] : 'no_image';
                          $dataProducts[$i][$j]['image_label'] = '';
                          $dataProducts[$i][$j]['thumbnail_label'] = '';
                          $dataProducts[$i][$j]['small_image_label'] = '';
                          $dataProducts[$i][$j]['status'] = Mage_Catalog_Model_Product_Status::STATUS_ENABLED;
                          $dataProducts[$i][$j]['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE;
                          $dataProducts[$i][$j]['tax_class_id'] = 0;
                          $dataProducts[$i][$j]['price'] = '';//$product['price']; // no price cz it's simple product generated fro using in configurable product
                          $dataProducts[$i][$j]['supplier_price'] = $product['price'];
                          $dataProducts[$i][$j]['tags'] = '';
                          $dataProducts[$i][$j]['special_price'] = '';
                          $dataProducts[$i][$j]['size'] = $prodSize;
                          $dataProducts[$i][$j]['color'] = strtolower($prodColor);
                          $dataProducts[$i][$j]['brand'] = trim($product['brand']) ? trim($product['brand']): '';
                          $dataProducts[$i][$j]['qty'] = $product['qty'][$j] ? $product['qty'][$j] : (int) $minQtyInStock;
                          $dataProducts[$i][$j]['is_in_stock'] = 1;
                          $dataProducts[$i][$j]['supplier_name'] = $supplierName;
                          $dataProducts[$i][$j]['options_container'] = 'container1';
                          $k = $j;
                        }
                      }

                      if(!empty($dataProducts[$i])) { // check if simple product array contain 2 or plus of products.
                        $q = $k + 1;
                        $simpleSkus = [];
                        foreach ($dataProducts[$i] as $key => $value) {
                          $simpleSkus[$key] = $value['sku'];
                        }
                        $this->trace('simple skus -->>',$q);

                        $dataProducts[$i][$q]['store'] = 'admin';
                        $dataProducts[$i][$q]['websites'] = 'base';
                        $dataProducts[$i][$q]['attribute_set'] = 'Default';
                        if($prodSize && $prodColor) {
                          $dataProducts[$i][$q]['configurable_attributes'] = 'color,size'; // all configurable product attribute used
                        }else if($prodColor) {
                          $dataProducts[$i][$q]['configurable_attributes'] = 'color'; // all configurable product attribute used
                        }else if($prodSize) {
                          $dataProducts[$i][$q]['configurable_attributes'] = 'size'; // all configurable product attribute used
                        }
                        $dataProducts[$i][$q]['type'] = 'configurable';
                        $dataProducts[$i][$q]['supplier_prod_name'] = trim($product['name']) ? substr(trim($product['name']), 0, $productNameMaxLength) : 'no_name';
                        $dataProducts[$i][$q]['name'] = $supplierName.' '. array_search(trim($product['category']),$categoryTranslation). ' '.array_search(trim($product['sub_category']),$categoryTranslation);
                        $dataProducts[$i][$q]['system_name'] = $supplierName.' '. array_search(trim($product['category']),$categoryTranslation). ' '.array_search(trim($product['sub_category']),$categoryTranslation);
                        $dataProducts[$i][$q]['product_name'] = trim($product['name']) ? substr(trim($product['name']), 0, $productNameMaxLength) : 'no_name';
                        $dataProducts[$i][$q]['categories'] = $categoryOfProd.''.$subCategoryOfProd;
                        $dataProducts[$i][$q]['simples_skus'] = implode(',',$simpleSkus);
                        $dataProducts[$i][$q]['sku'] = trim(strtolower($product['id'] ? str_replace(' ','-',$product['id']) : ''));
                        $dataProducts[$i][$q]['code'] = trim(strtolower($product['code'] ? str_replace(' ','-',$product['code']) : ''));
                        $dataProducts[$i][$q]['prod_id'] = is_numeric(trim($product['id'])) ? trim(strtolower($product['id'] ? $product['id'] : '')) : '';
                        $dataProducts[$i][$q]['has_options'] = '1'; // configurable product has_option default 1
                        $dataProducts[$i][$q]['news_from_date'] = '';
                        $dataProducts[$i][$q]['news_to_date'] = '';
                        $dataProducts[$i][$q]['weight'] = $defaultWeight; // Default 200 g ; Unit in gramme
                        $dataProducts[$i][$q]['description'] = '';//$product['description'];
                        $dataProducts[$i][$q]['short_description'] = '';//$product['description'];
                        $dataProducts[$i][$q]['meta_title'] = '';
                        $dataProducts[$i][$q]['meta_keyword'] = '';
                        $dataProducts[$i][$q]['meta_description'] = '';
                        $dataProducts[$i][$q]['url_key'] = '';
                        $dataProducts[$i][$q]['url_path'] = '';
                        $dataProducts[$i][$q]['image'] = $product['image'] ? '+'.$product['image'] : '';
                        $dataProducts[$i][$q]['media_gallery'] = implode(';',$mediaGalleyArray);
                        $dataProducts[$i][$q]['small_image'] = $product['image'] ? '+'.$product['image'] : '';
                        $dataProducts[$i][$q]['thumbnail'] = isset($mediaGalleyArray['image_gallery0']) ? $mediaGalleyArray['image_gallery0'] : ($product['image'] ? '+'.$product['image'] : '');
                        $dataProducts[$i][$q]['image_label'] = '';
                        $dataProducts[$i][$q]['thumbnail_label'] = '';
                        $dataProducts[$i][$q]['small_image_label'] = '';
                        $dataProducts[$i][$q]['status'] = Mage_Catalog_Model_Product_Status::STATUS_ENABLED;
                        $dataProducts[$i][$q]['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
                        $dataProducts[$i][$q]['tax_class_id'] = 0;
                        $dataProducts[$i][$q]['price'] = '';//$product['price']; // no price cz it's simple product generated fro using in configurable product
                        $dataProducts[$i][$q]['supplier_price'] = $product['price'];
                        $dataProducts[$i][$q]['tags'] = '';
                        $dataProducts[$i][$q]['special_price'] = '';
                        $dataProducts[$i][$q]['size'] = '';
                        $dataProducts[$i][$q]['color'] = '';
                        $dataProducts[$i][$q]['brand'] = trim($product['brand']) ? trim($product['brand']): '';
                        $dataProducts[$i][$q]['qty'] = '0'; // configurable product default qty 0
                        $dataProducts[$i][$q]['is_in_stock'] = '1';
                        $dataProducts[$i][$q]['supplier_name'] = $supplierName;
                        $dataProducts[$i][$q]['options_container'] = 'container1';
                      }
                    // Fix Array keys sort
                    $dataProducts[$i] = array_values($dataProducts[$i]);
                    /************/
                    if(!empty($dataProducts) && is_array($dataProducts)) { // check if product exist get the translated name
                      foreach ($dataProducts[$i] as $key => $value) {
                        $productSKU = $dataProducts[$i][$key]['sku'];
                        $_product = Mage::getModel('catalog/product')->loadByAttribute('sku',$productSKU);
                        if(!empty($_product)) {
                          $dataProducts[$i][$key]['name'] = $_product->getName();
                        }
                      }
                    }
                  /*****************  if the product have size and color as options *****************/
                  }else if(isset($product['options']) && is_array($product['options'])) {
                    $this->trace('configurable options attributes is array..');
                      $k = 0 ;
                      foreach ($product['options'] as $j => $valAttr) {
                          // Size Mapping
                          $prodSize = '';
                          if(!empty($sizeMapping) && is_array($sizeMapping)) {
                            $sizeVal = trim($product['options'][$j]) ? trim($product['options'][$j]) : '';
                            $sizeMap = array_search($sizeVal,$sizeMapping);
                            if ($sizeMap !== false) { // size Mapping Exist in Array
                              $prodSize = $sizeMap;
                            }else { // size Mapping not Exist in Array
                              $prodSize = trim($product['options'][$j]) ? trim($product['options'][$j]) : '';
                            }
                          }else { // size Mapping not Exist in Array
                            $prodSize = trim($product['options'][$j]) ? trim($product['options'][$j]) : '';
                          }
                          // if($prodSize == 'Default') {
                          //   $prodSize = '';
                          // }

                          // Get Color And Size From option tag after ':' character
                          $_prodOption = $prodSize;
                          $_prodSize = '';
                          $_prodOptionStr = substr($_prodOption, strpos($_prodOption, ":") + 1);
                          $arr = explode(":", $_prodOptionStr, 2);
                          $first = $arr[0];
                          $charaCount = substr_count($first, '-');
                          if($charaCount == 2) { // '-' exist two time
                            $_prodOptionBeforeSep = substr($first, 0, strpos($first, '-', strpos($first, '-')+1));
                            $lastString = substr(strstr(substr(strstr($first, "-"),1),"-"),1);
                            $_prodColor = $_prodOptionBeforeSep;
                            $_prodSize = $lastString;
                            $_prdQty = $arr[1];
                          }else if($charaCount == 1) { // '-' exist one time
                            $_prodOptionStr = substr($_prodOption, strpos($_prodOption, ":") + 1);
                            $arr = explode(":", $_prodOptionStr, 2);
                            $first = $arr[0];
                            $_prodOptionAfterSep = explode("-", $first, 2);
                            $_prodColor = $_prodOptionAfterSep[0];
                            $_prodSize = $_prodOptionAfterSep[1];
                            $_prdQty = $arr[1];
                          }

                          $prodSizeMap = $_prodSize;
                          if(!empty($sizeMapping) && is_array($sizeMapping)) {
                            $sizeVal = trim($prodSizeMap) ? trim($prodSizeMap) : '';
                            $sizeMap = array_search($sizeVal,$sizeMapping);
                            if ($sizeMap !== false) { // size Mapping Exist in Array
                              $prodSizeMap = $sizeMap;
                            }else { // size Mappingnot Exist in Array
                              $prodSizeMap = trim($prodSizeMap) ? trim($prodSizeMap) : '';
                            }
                          }else { // size Mappingnot Exist in Array
                            $prodSizeMap = trim($prodSizeMap) ? trim($prodSizeMap) : '';
                          }
                          // if($prodSizeMap == 'Default') {
                          //   $prodSizeMap = '';
                          // }

                          // Color Mapping
                          $prodColor = '';
                          if(!empty($colorMapping) && is_array($colorMapping)) {
                            $colorVal = $_prodColor ? $_prodColor : '';
                            $colorMap = array_search($colorVal,$colorMapping);
                            if ($colorMap !== false) { // size Mapping Exist in Array
                              $prodColor = $colorMap;
                            }else { // size Mappingnot Exist in Array
                              $prodColor = $_prodColor ? $_prodColor : '';
                            }
                          }else { // size Mappingnot Exist in Array
                            $prodColor = $_prodColor ? $_prodColor : '';
                          }

                          $quantityOfPrd = $_prdQty;
                          if($quantityOfPrd >= (int) $minQtyInStock) { // if qty of simple product > 5
                          $dataProducts[$i][$j]['store'] = 'admin';
                          $dataProducts[$i][$j]['websites'] = 'base';
                          $dataProducts[$i][$j]['attribute_set'] = 'Default';
                          $dataProducts[$i][$j]['configurable_attributes'] = '';
                          $dataProducts[$i][$j]['type'] = 'simple';
                          $dataProducts[$i][$j]['supplier_prod_name'] = trim($product['name']) ? substr(trim($product['name']), 0, $productNameMaxLength) : 'no_name';
                          $dataProducts[$i][$j]['name'] = $supplierName.' '. array_search(trim($product['category']),$categoryTranslation). ' '.array_search(trim($product['sub_category']),$categoryTranslation).' '.strtolower($prodColor).' '.strtolower($prodSizeMap);
                          $dataProducts[$i][$j]['system_name'] = $supplierName.' '. array_search(trim($product['category']),$categoryTranslation). ' '.array_search(trim($product['sub_category']),$categoryTranslation).' '.strtolower($prodColor).' '.strtolower($prodSize);
                          $dataProducts[$i][$j]['product_name'] = trim($product['name']) ? substr(trim($product['name']), 0, $productNameMaxLength) : 'no_name';
                          $dataProducts[$i][$j]['categories'] = $categoryOfProd.''.$subCategoryOfProd;
                          $dataProducts[$i][$j]['simples_skus'] = '';
                          $dataProducts[$i][$j]['sku'] = $product['sku'] ? $product['sku'].'-'.$prodColor.'-'.$prodSizeMap : '';
                          $dataProducts[$i][$j]['code'] = trim(strtolower($product['code'] ? str_replace(' ','-',$product['code']) : ''));
                          $dataProducts[$i][$j]['prod_id'] = is_numeric(trim($product['id'])) ? trim(strtolower($product['id'] ? $product['id'] : '')) : '';
                          $dataProducts[$i][$j]['has_options'] = '0'; // simple product has_option default 0
                          $dataProducts[$i][$j]['news_from_date'] = '';
                          $dataProducts[$i][$j]['news_to_date'] = '';
                          $dataProducts[$i][$j]['weight'] = $defaultWeight; // Default 200 g ; Unit in gramme
                          $dataProducts[$i][$j]['description'] = '';//$product['description'];
                          $dataProducts[$i][$j]['short_description'] = '';//$product['description'];
                          $dataProducts[$i][$j]['meta_title'] = '';
                          $dataProducts[$i][$j]['meta_keyword'] = '';
                          $dataProducts[$i][$j]['meta_description'] = '';
                          $dataProducts[$i][$j]['url_key'] = '';
                          $dataProducts[$i][$j]['url_path'] = '';
                          $dataProducts[$i][$j]['image'] = '';//$product['image'] ? '+'.$product['image'] : 'no_image';
                          $dataProducts[$i][$j]['media_gallery'] = '';//implode(';',$mediaGalleyArray);
                          $dataProducts[$i][$j]['small_image'] = '';//$product['image'] ? '+'.$product['image'] : 'no_image';
                          $dataProducts[$i][$j]['thumbnail'] = '';//$product['image'] ? '+'.$product['image'] : 'no_image';
                          $dataProducts[$i][$j]['image_label'] = '';
                          $dataProducts[$i][$j]['thumbnail_label'] = '';
                          $dataProducts[$i][$j]['small_image_label'] = '';
                          $dataProducts[$i][$j]['status'] = Mage_Catalog_Model_Product_Status::STATUS_ENABLED;
                          $dataProducts[$i][$j]['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE;
                          $dataProducts[$i][$j]['tax_class_id'] = 0;
                          $dataProducts[$i][$j]['price'] = '';//$product['price']; // no price cz it's simple product generated fro using in configurable product
                          $dataProducts[$i][$j]['supplier_price'] = $product['price'];
                          $dataProducts[$i][$j]['tags'] = '';
                          $dataProducts[$i][$j]['special_price'] = '';
                          $dataProducts[$i][$j]['size'] = $prodSizeMap;
                          $dataProducts[$i][$j]['color'] = strtolower($prodColor);
                          $dataProducts[$i][$j]['brand'] = trim($product['brand']) ? trim($product['brand']): '';
                          $dataProducts[$i][$j]['qty'] = $quantityOfPrd;
                          $dataProducts[$i][$j]['is_in_stock'] = 1;
                          $dataProducts[$i][$j]['supplier_name'] = $supplierName;
                          $dataProducts[$i][$j]['options_container'] = 'container1';
                          $k = $j;
                        }
                      }

                      if(!empty($dataProducts[$i])) { // check if simple product array contain 2 or plus of products.
                        $q = $k + 1;
                        $simpleSkus = [];
                        foreach ($dataProducts[$i] as $key => $value) {
                          $simpleSkus[$key] = $value['sku'];
                        }
                        $this->trace('simple skus -->>',$q);

                        $dataProducts[$i][$q]['store'] = 'admin';
                        $dataProducts[$i][$q]['websites'] = 'base';
                        $dataProducts[$i][$q]['attribute_set'] = 'Default';
                        if($prodSizeMap && $prodColor) {
                          $dataProducts[$i][$q]['configurable_attributes'] = 'color,size'; // all configurable product attribute used
                        }else if($prodColor) {
                          $dataProducts[$i][$q]['configurable_attributes'] = 'color'; // all configurable product attribute used
                        }else if($prodSizeMap) {
                          $dataProducts[$i][$q]['configurable_attributes'] = 'size'; // all configurable product attribute used
                        }
                        $dataProducts[$i][$q]['type'] = 'configurable';
                        $dataProducts[$i][$q]['supplier_prod_name'] = trim($product['name']) ? substr(trim($product['name']), 0, $productNameMaxLength) : 'no_name';
                        $dataProducts[$i][$q]['name'] = $supplierName.' '. array_search(trim($product['category']),$categoryTranslation). ' '.array_search(trim($product['sub_category']),$categoryTranslation);
                        $dataProducts[$i][$q]['system_name'] = $supplierName.' '. array_search(trim($product['category']),$categoryTranslation). ' '.array_search(trim($product['sub_category']),$categoryTranslation);
                        $dataProducts[$i][$q]['product_name'] = trim($product['name']) ? substr(trim($product['name']), 0, $productNameMaxLength) : 'no_name';
                        $dataProducts[$i][$q]['categories'] = $categoryOfProd.''.$subCategoryOfProd;
                        $dataProducts[$i][$q]['simples_skus'] = implode(',',$simpleSkus);
                        $dataProducts[$i][$q]['sku'] = $product['sku'] ? $product['sku'] : '';
                        $dataProducts[$i][$q]['code'] = trim(strtolower($product['code'] ? str_replace(' ','-',$product['code']) : ''));
                        $dataProducts[$i][$q]['prod_id'] = is_numeric(trim($product['id'])) ? trim(strtolower($product['id'] ? $product['id'] : '')) : '';
                        $dataProducts[$i][$q]['has_options'] = '1'; // configurable product has_option default 1
                        $dataProducts[$i][$q]['news_from_date'] = '';
                        $dataProducts[$i][$q]['news_to_date'] = '';
                        $dataProducts[$i][$q]['weight'] = $defaultWeight; // Default 200 g ; Unit in gramme
                        $dataProducts[$i][$q]['description'] = '';//$product['description'];
                        $dataProducts[$i][$q]['short_description'] = '';//$product['description'];
                        $dataProducts[$i][$q]['meta_title'] = '';
                        $dataProducts[$i][$q]['meta_keyword'] = '';
                        $dataProducts[$i][$q]['meta_description'] = '';
                        $dataProducts[$i][$q]['url_key'] = '';
                        $dataProducts[$i][$q]['url_path'] = '';
                        $dataProducts[$i][$q]['image'] = $product['image'] ? '+'.$product['image'] : '';
                        $dataProducts[$i][$q]['media_gallery'] = implode(';',$mediaGalleyArray);
                        $dataProducts[$i][$q]['small_image'] = $product['image'] ? '+'.$product['image'] : '';
                        $dataProducts[$i][$q]['thumbnail'] = isset($mediaGalleyArray['image_gallery0']) ? $mediaGalleyArray['image_gallery0'] : ($product['image'] ? '+'.$product['image'] : '');
                        $dataProducts[$i][$q]['image_label'] = '';
                        $dataProducts[$i][$q]['thumbnail_label'] = '';
                        $dataProducts[$i][$q]['small_image_label'] = '';
                        $dataProducts[$i][$q]['status'] = Mage_Catalog_Model_Product_Status::STATUS_ENABLED;
                        $dataProducts[$i][$q]['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
                        $dataProducts[$i][$q]['tax_class_id'] = 0;
                        $dataProducts[$i][$q]['price'] = '';//$product['price']; // no price cz it's simple product generated fro using in configurable product
                        $dataProducts[$i][$q]['supplier_price'] = $product['price'];
                        $dataProducts[$i][$q]['tags'] = '';
                        $dataProducts[$i][$q]['special_price'] = '';
                        $dataProducts[$i][$q]['size'] = '';
                        $dataProducts[$i][$q]['color'] = '';
                        $dataProducts[$i][$q]['brand'] = trim($product['brand']) ? trim($product['brand']): '';
                        $dataProducts[$i][$q]['qty'] = '0'; // configurable product default qty 0
                        $dataProducts[$i][$q]['is_in_stock'] = '1';
                        $dataProducts[$i][$q]['supplier_name'] = $supplierName;
                        $dataProducts[$i][$q]['options_container'] = 'container1';
                      }
                      // Fix Array keys sort
                      $dataProducts[$i] = array_values($dataProducts[$i]);
                      /************/
                      if(!empty($dataProducts) && is_array($dataProducts)) { // check if product exist get the translated name
                        foreach ($dataProducts[$i] as $key => $value) {
                          $productSKU = $dataProducts[$i][$key]['sku'];
                          $_product = Mage::getModel('catalog/product')->loadByAttribute('sku',$productSKU);
                          if(!empty($_product)) {
                            $dataProducts[$i][$key]['name'] = $_product->getName();
                          }
                        }
                      }
                      /************/
                  }// end options foreach
                }else { // Default simple product
                  // TO DO...
                  $this->trace('Generate Default Simple Product');

                  $quantityOfSimpPrd = isset($product['qty']) ? $product['qty'] : (int) $minQtyInStock;

                  if($quantityOfSimpPrd >= (int) $minQtyInStock) {
                    $dataProducts[$i]['store'] = 'admin';
                    $dataProducts[$i]['websites'] = 'base';
                    $dataProducts[$i]['attribute_set'] = 'Default';
                    $dataProducts[$i]['configurable_attributes'] = '';
                    $dataProducts[$i]['type'] = 'simple';
                    $dataProducts[$i]['supplier_prod_name'] = trim($product['name']) ? substr(trim($product['name']), 0, $productNameMaxLength) : 'no_name';
                    $dataProducts[$i]['name'] = $supplierName.' '. array_search(trim($product['category']),$categoryTranslation). ' '.array_search(trim($product['sub_category']),$categoryTranslation);
                    $dataProducts[$i]['system_name'] = $supplierName.' '. array_search(trim($product['category']),$categoryTranslation). ' '.array_search(trim($product['sub_category']),$categoryTranslation);
                    $dataProducts[$i]['product_name'] = trim($product['name']) ? substr(trim($product['name']), 0, $productNameMaxLength) : 'no_name';
                    $dataProducts[$i]['categories'] = $categoryOfProd.''.$subCategoryOfProd;
                    $dataProducts[$i]['simples_skus'] = '';
                    $dataProducts[$i]['sku'] = $product['sku'] ? trim($product['sku']) : trim(strtolower($product['code'] ? str_replace(' ','-',$product['code']) : ''));
                    $dataProducts[$i]['code'] = trim(strtolower($product['code'] ? str_replace(' ','-',$product['code']) : ''));
                    $dataProducts[$i]['prod_id'] = is_numeric(trim($product['id'])) ? trim(strtolower($product['id'] ? $product['id'] : '')) : '';
                    $dataProducts[$i]['has_options'] = '0'; // simple product has_option default 0
                    $dataProducts[$i]['news_from_date'] = '';
                    $dataProducts[$i]['news_to_date'] = '';
                    $dataProducts[$i]['weight'] = $defaultWeight; // Default 200 g ; Unit in gramme
                    $dataProducts[$i]['description'] = '';//$product['description'] ? $product['description'] : '' ;
                    $dataProducts[$i]['short_description'] = '';//$product['description'] ? $product['description'] : '' ;
                    $dataProducts[$i]['meta_title'] = '';
                    $dataProducts[$i]['meta_keyword'] = '';
                    $dataProducts[$i]['meta_description'] = '';
                    $dataProducts[$i]['url_key'] = '';
                    $dataProducts[$i]['url_path'] = '';
                    $dataProducts[$i]['image'] = $product['image'] ? '+'.$product['image'] : '';
                    $dataProducts[$i]['media_gallery'] = implode(';',$mediaGalleyArray);
                    $dataProducts[$i]['small_image'] = $product['image'] ? '+'.$product['image'] : '';
                    $dataProducts[$i]['thumbnail'] = isset($mediaGalleyArray['image_gallery0']) ? $mediaGalleyArray['image_gallery0'] : ($product['image'] ? '+'.$product['image'] : '');
                    $dataProducts[$i]['image_label'] = '';
                    $dataProducts[$i]['thumbnail_label'] = '';
                    $dataProducts[$i]['small_image_label'] = '';
                    $dataProducts[$i]['status'] = Mage_Catalog_Model_Product_Status::STATUS_ENABLED;
                    $dataProducts[$i]['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
                    $dataProducts[$i]['tax_class_id'] = 0;
                    $dataProducts[$i]['price'] = '';//$product['price'] ? $product['price'] : 'no price'; // no price cz it's simple product generated fro using in configurable product
                    $dataProducts[$i]['supplier_price'] = $product['price'];
                    $dataProducts[$i]['tags'] = '';
                    $dataProducts[$i]['special_price'] = '';
                    $dataProducts[$i]['size'] = '';
                    $dataProducts[$i]['color'] = '';
                    $dataProducts[$i]['brand'] = trim($product['brand']) ? trim($product['brand']): '';
                    $dataProducts[$i]['qty'] = $quantityOfSimpPrd;
                    $dataProducts[$i]['is_in_stock'] = '1';
                    $dataProducts[$i]['supplier_name'] = $supplierName;
                    $dataProducts[$i]['options_container'] = 'container1';
                  }

                  /************/
                  if(!empty($dataProducts) && is_array($dataProducts)) { // check if product exist get the translated name
                    foreach ($dataProducts[$i] as $key => $value) {
                      $productSKU = $dataProducts[$i][$key]['sku'];
                      $_product = Mage::getModel('catalog/product')->loadByAttribute('sku',$productSKU);
                      if(!empty($_product)) {
                        $dataProducts[$i][$key]['name'] = $_product->getName();
                      }
                    }
                  }
                  /************/

                }
                //$this->trace('Array of Simple products',$dataProducts);
            } // end foreach

            $this->trace(' --->>> Data products',array_filter($dataProducts));
        }
        return array_values(array_filter($dataProducts));    }    /**     * Build XML Data     *     * @access protected     * @param $data , $prefix     * @return     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com     */    protected function build($dataProd, $prefix)    {      $list = array();      $data = $this->buildPathLoop($dataProd,$prefix);      if(!empty($data) && is_array($data)){          foreach($data as $attribute => $items){              if(!empty($items) && is_array($items)){                  foreach($items as $index => $value){                      if(!isset($list[$index])){                          $list[$index] = [];                      }                      $list[$index][$attribute] = $value;                  }              }          }      }      $this->trace('Loop Result', $list);      return $list;    }    /**     * Build XML Data     *     * @access protected     * @param $data, $prefix     * @return     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com     */    protected function buildPathLoop($data, $prefix, $initialIndex = '', &$list = [])    {        if (!empty($data) && is_array($data)) {            foreach ($data AS $key => $value) {                $index = $initialIndex;                if(!is_numeric($key)){                  $index .= '.' . $key;                }                if (!empty($value) && is_array($value)) {                    $index .= '.*';                    foreach ($value as $subKey => $subValue) {                      $indo = $index;                      if(!is_numeric($subKey)) {                          $indo .= '.' . $subKey;                      }                      if(!empty($subValue) && is_array($subValue)) {                          $this->buildPathLoop($subValue,$prefix, $indo, $list);                      }else{ // if the value have one sub array                          if(!is_numeric($subKey)) {                            if (0 === strpos($indo, '.')) {                              $indo = $prefix.''.substr_replace($indo, '', 0, 1);                            }                            if (!isset($list[$indo])) {                                $list[$indo] = [];                            }                            $list[$indo][] = $subValue ? $subValue : '';                          }else {                            if (0 === strpos($indo, '.')) {                                $indo = $prefix.''.'*'.str_replace(".*","",$indo);                            }                            if (!isset($list[$indo])) {                                $list[$indo] = [];                            }                            $list[$indo][] = $subValue ? $subValue : '';                          }                      }                    }                } else {                    if (0 === strpos($index, '.')) {                        $index = $prefix.''.substr_replace($index, '', 0, 1);                    }                    if (!isset($list[$index])) {                        $list[$index] = [];                    }                    $list[$index][] = $value ? $value : '';                }            }        }        return $list;    }    /**     * Magento Find Default Attribute Set Id for product     *     * @access protected     * @param null     * @return $sDefaultAttributeSetId     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com     */    protected function findDefaultAttributeSetId() {      // Find the attribute set id      $sDefaultAttributeSetId = Mage::getSingleton('eav/config')      ->getEntityType(Mage_Catalog_Model_Product::ENTITY)      ->getDefaultAttributeSetId();      return $sDefaultAttributeSetId;    }    /**     * Curl Get File Contents     *     * @access protected     * @param $xmlPath     * @return $contents     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com     */    protected function curl_get_file_contents($xmlPath) {      $curl = curl_init();      curl_setopt($curl, CURLOPT_URL, $xmlPath);      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);      // attempt to retrieve the modification date      //curl_setopt($curl, CURLOPT_FILETIME, true);      $contents = curl_exec($curl);      if ($contents === false) {          die (curl_error($curl));          Mage::log($this->debugFileName().' - Message: Curl error get Content false', null, 'picksellers.log');      }      curl_close($curl);      return $contents;    }}