<?php
/**
 * CodenDot_PickSel extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       CodenDot
 * @package        CodenDot_PickSel
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier admin grid block
 *
 * @category    CodenDot
 * @package     CodenDot_PickSel
 * @author      Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
 */
class CodenDot_PickSel_Block_Adminhtml_Supplier_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('supplierGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return CodenDot_PickSel_Block_Adminhtml_Supplier_Grid
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('codendot_picksel/supplier')
            ->getCollection()
            ->addAttributeToSelect('supplier_name')
            ->addAttributeToSelect('supplier_url')
            ->addAttributeToSelect('supplier_address')
            ->addAttributeToSelect('supplier_time_update')
            ->addAttributeToSelect('status');

        $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
        $store = $this->_getStore();
        $collection->joinAttribute(
            'supplier_new_price',
            'codendot_picksel_supplier/supplier_new_price',
            'entity_id',
            null,
            'inner',
            $adminStore
        );
        if ($store->getId()) {
            $collection->joinAttribute(
                'codendot_picksel_supplier_supplier_new_price',
                'codendot_picksel_supplier/supplier_new_price',
                'entity_id',
                null,
                'inner',
                $store->getId()
            );
        }

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return CodenDot_PickSel_Block_Adminhtml_Supplier_Grid
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('codendot_picksel')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'supplier_new_price',
            array(
                'header'    => Mage::helper('codendot_picksel')->__('New Price'),
                'align'     => 'left',
                'index'     => 'supplier_new_price',
            )
        );

        if ($this->_getStore()->getId()) {
            $this->addColumn(
                'codendot_picksel_supplier_supplier_new_price',
                array(
                    'header'    => Mage::helper('codendot_picksel')->__('New Price in %s', $this->_getStore()->getName()),
                    'align'     => 'left',
                    'index'     => 'codendot_picksel_supplier_supplier_new_price',
                )
            );
        }

        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('codendot_picksel')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    '1' => Mage::helper('codendot_picksel')->__('Enabled'),
                    '0' => Mage::helper('codendot_picksel')->__('Disabled'),
                )
            )
        );
        $this->addColumn(
            'supplier_name',
            array(
                'header' => Mage::helper('codendot_picksel')->__('Name'),
                'index'  => 'supplier_name',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'supplier_url',
            array(
                'header' => Mage::helper('codendot_picksel')->__('URL'),
                'index'  => 'supplier_url',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'supplier_address',
            array(
                'header' => Mage::helper('codendot_picksel')->__('Address'),
                'index'  => 'supplier_address',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'supplier_time_update',
            array(
                'header' => Mage::helper('codendot_picksel')->__('Supplier time Update'),
                'index'  => 'supplier_time_update',
                'type'  => 'options',
                'options' => Mage::helper('codendot_picksel')->convertOptions(
                    Mage::getModel('eav/config')->getAttribute('codendot_picksel_supplier', 'supplier_time_update')->getSource()->getAllOptions(false)
                )

            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('codendot_picksel')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('codendot_picksel')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('codendot_picksel')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('codendot_picksel')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    ),
                    array(
                        'caption' => Mage::helper('codendot_picksel')->__('Field Translations'),
                        'url'     => array('base'=> '*/*/suppliertranslations'),
                        'field'   => 'id'
                    ),
                    array(
                        'caption' => Mage::helper('codendot_picksel')->__('Category Translations'),
                        'url'     => array('base'=> '*/*/categorytranslations'),
                        'field'   => 'id'
                    ),
                    array(
                        'caption' => Mage::helper('codendot_picksel')->__('Category Mapping'),
                        'url'     => array('base'=> '*/*/categoriesmapping'),
                        'field'   => 'id'
                    ),
                    array(
                        'caption' => Mage::helper('codendot_picksel')->__('Field Mapping'),
                        'url'     => array('base'=> '*/*/suppliermapping'),
                        'field'   => 'id'
                    ),
                    array(
                        'caption' => Mage::helper('codendot_picksel')->__('Run'),
                        'url'     => array('base'=> '*/*/supplierfetchproducts'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('codendot_picksel')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('codendot_picksel')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('codendot_picksel')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * get the selected store
     *
     * @access protected
     * @return Mage_Core_Model_Store
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return CodenDot_PickSel_Block_Adminhtml_Supplier_Grid
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('supplier');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('codendot_picksel')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('codendot_picksel')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('codendot_picksel')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('codendot_picksel')->__('Status'),
                        'values' => array(
                            '1' => Mage::helper('codendot_picksel')->__('Enabled'),
                            '0' => Mage::helper('codendot_picksel')->__('Disabled'),
                        )
                    )
                )
            )
        );
        $this->getMassactionBlock()->addItem(
            'supplier_time_update',
            array(
                'label'      => Mage::helper('codendot_picksel')->__('Change Supplier time Update'),
                'url'        => $this->getUrl('*/*/massSupplierTimeUpdate', array('_current'=>true)),
                'additional' => array(
                    'flag_supplier_time_update' => array(
                        'name'   => 'flag_supplier_time_update',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('codendot_picksel')->__('Supplier time Update'),
                        'values' => Mage::getModel('eav/config')->getAttribute('codendot_picksel_supplier', 'supplier_time_update')
                            ->getSource()->getAllOptions(true),

                    )
                )
            )
        );

        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param CodenDot_PickSel_Model_Supplier
     * @return string
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}
