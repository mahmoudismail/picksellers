<?php
/**
 * CodenDot_PickSel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       CodenDot
 * @package        CodenDot_PickSel
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Adminhtml supplier attribute edit page tabs
 *
 * @category    CodenDot
 * @package     CodenDot_PickSel
 * @author      Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
 */
class CodenDot_PickSel_Block_Adminhtml_Supplier_Attribute_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * constructor
     *
     * @access public
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('supplier_attribute_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('codendot_picksel')->__('Attribute Information'));
    }

    /**
     * add attribute tabs
     *
     * @access protected
     * @return CodenDot_PickSel_Adminhtml_Supplier_Attribute_Edit_Tabs
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'main',
            array(
                'label'     => Mage::helper('codendot_picksel')->__('Properties'),
                'title'     => Mage::helper('codendot_picksel')->__('Properties'),
                'content'   => $this->getLayout()->createBlock(
                    'codendot_picksel/adminhtml_supplier_attribute_edit_tab_main'
                )
                ->toHtml(),
                'active'    => true
            )
        );
        $this->addTab(
            'labels',
            array(
                'label'     => Mage::helper('codendot_picksel')->__('Manage Label / Options'),
                'title'     => Mage::helper('codendot_picksel')->__('Manage Label / Options'),
                'content'   => $this->getLayout()->createBlock(
                    'codendot_picksel/adminhtml_supplier_attribute_edit_tab_options'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }
}
