<?php
/**
 * CodenDot_PickSel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       CodenDot
 * @package        CodenDot_PickSel
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier admin edit form
 *
 * @category    CodenDot
 * @package     CodenDot_PickSel
 * @author      Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
 */
class CodenDot_PickSel_Block_Adminhtml_Supplier_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'codendot_picksel';
        $this->_controller = 'adminhtml_supplier';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('codendot_picksel')->__('Save Supplier')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('codendot_picksel')->__('Delete Supplier')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('codendot_picksel')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_supplier') && Mage::registry('current_supplier')->getId()) {
            return Mage::helper('codendot_picksel')->__(
                "Edit Supplier '%s'",
                $this->escapeHtml(Mage::registry('current_supplier')->getSupplierNewPrice())
            );
        } else {
            return Mage::helper('codendot_picksel')->__('Add Supplier');
        }
    }
}
