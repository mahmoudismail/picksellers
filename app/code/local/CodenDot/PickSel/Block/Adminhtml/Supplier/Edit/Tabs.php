<?php
/**
 * CodenDot_PickSel extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       CodenDot
 * @package        CodenDot_PickSel
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier admin edit tabs
 *
 * @category    CodenDot
 * @package     CodenDot_PickSel
 * @author      Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
 */
class CodenDot_PickSel_Block_Adminhtml_Supplier_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('supplier_info_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('codendot_picksel')->__('Supplier Information'));
    }

    /**
     * prepare the layout
     *
     * @access protected
     * @return CodenDot_PickSel_Block_Adminhtml_Supplier_Edit_Tabs
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    protected function _prepareLayout()
    {
        $supplier = $this->getSupplier();
        $entity = Mage::getModel('eav/entity_type')
            ->load('codendot_picksel_supplier', 'entity_type_code');
        $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setEntityTypeFilter($entity->getEntityTypeId());
        $attributes->getSelect()->order('additional_table.position', 'ASC');

        $this->addTab(
            'info',
            array(
                'label'   => Mage::helper('codendot_picksel')->__('Supplier Information'),
                'content' => $this->getLayout()->createBlock(
                    'codendot_picksel/adminhtml_supplier_edit_tab_attributes'
                )
                ->setAttributes($attributes)
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve supplier entity
     *
     * @access public
     * @return CodenDot_PickSel_Model_Supplier
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function getSupplier()
    {
        return Mage::registry('current_supplier');
    }
}
