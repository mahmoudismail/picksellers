<?php
/**
 * CodenDot_PickSel extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       CodenDot
 * @package        CodenDot_PickSel
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier admin block
 *
 * @category    CodenDot
 * @package     CodenDot_PickSel
 * @author      Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
 */
class CodenDot_PickSel_Block_Adminhtml_Supplier extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Mahmoud ismail | tel: +96170800335 | mail: mahmoud@codendot.com
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_supplier';
        $this->_blockGroup         = 'codendot_picksel';
        parent::__construct();
        $this->_headerText         = Mage::helper('codendot_picksel')->__('Supplier');
        $this->_updateButton('add', 'label', Mage::helper('codendot_picksel')->__('Add Supplier'));

        $this->setTemplate('codendot_picksel/grid.phtml');
    }
}
